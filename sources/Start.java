/**
 * Before we begin, we must put ourselves into the right mindset.
 * <prayer>
 * Our father in heaven
 * hallowed be thy name
 * Your project come,
 * your code be done,
 * in Java, and in Objective-C
 * Give us this day our daily caffeine,
 * and forgive us our bugs,
 * as we have also forgiven our IDE
 * And lead us not to error
 * but deliver us from messy code.
 * For thine is the source code,
 * and the compiler, and the interpreter,
 * for ever.
 * Amen.
 * </prayer>
 */
import java.io.File;

import elektoh.game.Elektoh;

public class Start {
    private static String nativeDir = File.separator + "libraries" + File.separator + "native";
    private static File gameDirectory;

    public static void main(String... args) {
        gameDirectory = new File(args[0]);
        setSystemProperties();
        launch();
    }

    private static void setSystemProperties() {
        System.setProperty("org.lwjgl.opengl.Display.enableOSXFullscreenModeAPI", "true");
        System.setProperty("org.lwjgl.librarypath", gameDirectory.getAbsolutePath() + nativeDir);
    }

    private static void launch() {
        Elektoh.create(gameDirectory);
    }
}
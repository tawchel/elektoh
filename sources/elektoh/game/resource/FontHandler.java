package elektoh.game.resource;

import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import elektoh.game.utils.Directories;

public class FontHandler {
    public static final int FONT_SCALE = 10;
    public static TrueTypeFont FONT_TAWCHEL;

    public static void loadFonts(String id) throws FontFormatException, IOException {
        FONT_TAWCHEL = new TrueTypeFont(id, "tawchel-type");
        FONT_TAWCHEL.create();
    }

    public static File getFont(String id, String name) {
        return ResourceManager.getFile(Directories.fonts(id) + "/" + name + ".ttf");
    }

    public static InputStream getFontStream(String id, String name) {
        return ResourceManager.getResourceAsStream(getFont(id, name));
    }
}
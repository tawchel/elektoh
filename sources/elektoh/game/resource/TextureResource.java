package elektoh.game.resource;

import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;

public class TextureResource {
    private int id;
    private int refCount;
    private int width;
    private int height;

    public TextureResource() {
        id = glGenTextures();
        refCount = 1;
    }

    @Override
    protected void finalize() {
        glDeleteBuffers(id);
    }

    public void addRef() {
        refCount++;
    }

    public boolean removeRef() {
        refCount--;
        return refCount == 0;
    }

    public int getId() {
        return id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
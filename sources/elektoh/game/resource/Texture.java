package elektoh.game.resource;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;

import javax.imageio.ImageIO;

import elektoh.game.utils.MathUtils;

public class Texture {
    private static HashMap<String, TextureResource> loadedTextures = new HashMap<String, TextureResource>();
    private TextureResource resource;
    private String fileName;

    public Texture(String fileName) {
        this.fileName = fileName;
        TextureResource oldResource = loadedTextures.get(fileName);
        if (oldResource != null) {
            resource = oldResource;
            resource.addRef();
        } else {
            resource = loadTexture(fileName);
            loadedTextures.put(fileName, resource);
        }
    }

    public Texture(BufferedImage b, String name) {
        fileName = name;
        TextureResource oldResource = loadedTextures.get(fileName);
        if (oldResource != null) {
            resource = oldResource;
            resource.addRef();
        } else {
            resource = loadTexture(b);
            loadedTextures.put(fileName, resource);
        }
    }

    public int getID() {
        return resource.getId();
    }

    public void bind() {
        bind(0);
    }

    public void bind(int samplerSlot) {
        assert ((samplerSlot >= 0) && (samplerSlot <= 31));
        glActiveTexture(GL_TEXTURE0 + samplerSlot);
        glBindTexture(GL_TEXTURE_2D, resource.getId());
    }

    private static TextureResource loadTexture(String fileName) {
        try {
            return loadTexture(ImageIO.read(ResourceManager.getFile(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static TextureResource loadTexture(BufferedImage image) {
        int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        ByteBuffer buffer = MathUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);
        boolean hasAlpha = image.getColorModel().hasAlpha();
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int pixel = pixels[(y * image.getWidth()) + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));
                buffer.put((byte) ((pixel >> 8) & 0xFF));
                buffer.put((byte) ((pixel) & 0xFF));
                if (hasAlpha) {
                    buffer.put((byte) ((pixel >> 24) & 0xFF));
                } else {
                    buffer.put((byte) (0xFF));
                }
            }
        }
        buffer.flip();
        TextureResource resource = new TextureResource();
        glBindTexture(GL_TEXTURE_2D, resource.getId());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
        resource.setWidth(image.getWidth());
        resource.setHeight(image.getHeight());
        return resource;
    }

    public int getWidth() {
        return resource.getWidth();
    }

    public int getHeight() {
        return resource.getHeight();
    }

    @Override
    public void finalize() {
        if (resource.removeRef() && !fileName.isEmpty()) {
            loadedTextures.remove(fileName);
        }
    }
}
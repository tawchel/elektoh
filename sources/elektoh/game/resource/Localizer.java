package elektoh.game.resource;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import elektoh.game.utils.Directories;

public class Localizer {
    private static Map<String, String> localizations = new HashMap<String, String>();

    public static void loadLocalizations(String id, String language) throws IOException {
        localizations.clear();
        File localization = ResourceManager.getFile(Directories.language(id) + "/" + language);
        for (String s : localization.list()) {
            loadFileLocalizations(new File(localization.getAbsolutePath() + "/" + s));
        }
    }

    private static void loadFileLocalizations(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.startsWith("#")) {
                continue;
            }
            if (!line.contains("=")) {
                continue;
            }
            int index = line.indexOf("=");
            String key = line.substring(0, index);
            String value = line.substring(index + 1);
            localizations.put(key, value);
        }
        reader.close();
    }

    public static String localize(String s) {
        if (!localizations.containsKey(s)) {
            return s;
        }
        return localizations.get(s);
    }
}
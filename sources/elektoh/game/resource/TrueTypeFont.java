package elektoh.game.resource;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import elektoh.game.resource.sprite.Sprite;
import elektoh.game.resource.sprite.SpriteStill;
import elektoh.game.utils.Color;

/**
 * This class does not support extra characters & anti alias. See the link below for another implementation of TTF loading.
 * > http://lwjgl.org/forum/index.php/topic,2951.0.html
 * Also look at this for conversion information.
 * > http://stackoverflow.com/questions/139655/convert-pixels-to-points
 */
public class TrueTypeFont {
    public static double CONVERSION = 1d / 192d;
    public Sprite[] charArray = new Sprite[256];
    public final String name;
    public final Font font;
    public final FontMetrics metrics;
    private Texture fontTexture;

    public TrueTypeFont(String id, String name) throws FontFormatException, IOException {
        this.name = name;
        InputStream inputStream = FontHandler.getFontStream(id, name);
        font = Font.createFont(Font.TRUETYPE_FONT, inputStream).deriveFont(144f);
        BufferedImage tmpImg = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) tmpImg.getGraphics();
        g.setFont(font);
        metrics = g.getFontMetrics();
    }

    public void create() {
        int maxCharWidth = 0;
        for (int width : metrics.getWidths()) {
            if (width > maxCharWidth) {
                maxCharWidth = width;
            }
        }
        BufferedImage charMap = new BufferedImage(32 * maxCharWidth, 8 * metrics.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) charMap.getGraphics();
        for (int row = 0; row < 8; row++) {
            for (int column = 0; column < 32; column++) {
                char c = (char) ((32 * row) + column);
                BufferedImage charImage = getCharImage(c, metrics.getHeight());
                if (charImage != null) {
                    g.drawImage(charImage, column * maxCharWidth, (row * metrics.getHeight()) - metrics.getDescent(), null);
                }
            }
        }
        fontTexture = new Texture(charMap, "font." + name);
        for (int row = 0; row < 8; row++) {
            for (int column = 0; column < 32; column++) {
                char c = (char) ((32 * row) + column);
                charArray[c] = new SpriteStill(fontTexture, column * maxCharWidth, (row * metrics.getHeight()) - metrics.getDescent(), metrics.charWidth(c), metrics.getHeight());
            }
        }
    }

    private BufferedImage getCharImage(char c, int maxCharHeight) {
        if (metrics.charWidth(c) == 0) {
            return null;
        }
        BufferedImage charImage = new BufferedImage(metrics.charWidth(c), maxCharHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) charImage.getGraphics();
        g.setFont(font);
        g.setColor(Color.WHITE.toAwt());
        g.drawString(String.valueOf(c), 0, metrics.getHeight() - metrics.getDescent());
        return charImage;
    }

    public void drawString(String string, int size, Color color) {
        glPushMatrix();
        int prevCharWidth = 0;
        glScaled(size * CONVERSION, size * CONVERSION, 0);
        for (int i = 0; i < string.length(); i++) {
            glTranslatef(prevCharWidth, 0, 0);
            char c = string.charAt(i);
            if (c >= charArray.length) {
                c = '0';
            }
            charArray[c].render(color);
            prevCharWidth = metrics.charWidth(c);
        }
        glPopMatrix();
    }

    public double getStringWidth(String string, int size) {
        double stringWidth = 0;
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            stringWidth += metrics.charWidth(c) * size * CONVERSION;
        }
        return stringWidth;
    }

    public double getFontHeight(int size) {
        return (metrics.getHeight() - metrics.getDescent()) * size * CONVERSION;
    }
}
package elektoh.game.resource;

import java.awt.FontFormatException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

import elektoh.game.Elektoh;
import elektoh.game.resource.sprite.SpriteAnimated;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.resource.sprite.load.LoadCharacter;
import elektoh.game.resource.sprite.load.LoadDecor;
import elektoh.game.resource.sprite.load.LoadTile;
import elektoh.game.resource.sprite.load.LoadUI;
import elektoh.game.settings.SettingsHandler;

public class ResourceManager {
    public static void loadResources() {
        try {
            doLoadResources();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void doLoadResources() throws IOException, FontFormatException {
        File assetsDir = getFile("assets");
        if (assetsDir != null) {
            for (File assetDir : assetsDir.listFiles()) {
                String id = assetDir.getName();
                FontHandler.loadFonts(id);
                Localizer.loadLocalizations(id, SettingsHandler.settings.language);
                loadSprites(id);
            }
        }
    }

    private static void loadSprites(String id) {
        SpriteAnimated.reset();
        SpriteDatabase.initializeSheets();
        LoadUI.loadSprites(id);
        LoadTile.loadSprites(id);
        LoadCharacter.loadSprites(id);
        LoadDecor.loadSprites(id);
    }

    public static URL getResource(String name) {
        return Elektoh.class.getClassLoader().getResource(name);
    }

    public static File getFile(String name) {
        File tmp = null;
        try {
            tmp = new File(getResource(name).toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return tmp;
    }

    public static InputStream getResourceAsStream(File file) {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}

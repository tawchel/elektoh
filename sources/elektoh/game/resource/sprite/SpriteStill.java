package elektoh.game.resource.sprite;

import elektoh.game.resource.Texture;
import elektoh.game.utils.QuadSelector;

public class SpriteStill extends Sprite {
    private final Texture texture;
    public final QuadSelector region;

    public SpriteStill(Texture tex, int x, int y, int width, int height) {
        this(tex, new QuadSelector(x, y, width, height));
    }

    public SpriteStill(Texture tex, QuadSelector region) {
        texture = tex;
        this.region = region;
    }

    @Override
    public Texture getTexture() {
        return texture;
    }

    @Override
    public QuadSelector getRegion() {
        return region;
    }
}
package elektoh.game.resource.sprite;

import java.util.ArrayList;
import java.util.List;

import elektoh.game.resource.Texture;
import elektoh.game.utils.QuadSelector;
import elektoh.game.utils.geometry.Coord;

public class SpriteAnimated extends SpriteStill {
    private static List<SpriteAnimated> animatedSprites = new ArrayList<SpriteAnimated>();
    private final int updateSpeed;
    public final QuadSelector[] regions;
    private int currentRegion = 0;

    public static void update(int gameTime) {
        for (SpriteAnimated sprite : animatedSprites) {
            if ((gameTime % sprite.updateSpeed) == 0) {
                sprite.currentRegion++;
                if (sprite.currentRegion >= sprite.regions.length) {
                    sprite.currentRegion = 0;
                }
            }
        }
    }

    public static void reset() {
        animatedSprites.clear();
    }

    public SpriteAnimated(Texture tex, int updateSpeed, QuadSelector... regions) {
        super(tex, regions[0]);
        this.regions = regions;
        this.updateSpeed = updateSpeed;
        animatedSprites.add(this);
    }

    @Override
    public QuadSelector getRegion() {
        return regions[currentRegion];
    }

    public static Coord[] animationBuilderY(int x, int... intArray) {
        Coord[] cArray = new Coord[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            cArray[i] = new Coord(x, intArray[i]);
        }
        return cArray;
    }

    public static Coord[] animationBuilderX(int y, int... intArray) {
        Coord[] cArray = new Coord[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            cArray[i] = new Coord(intArray[i], y);
        }
        return cArray;
    }
}
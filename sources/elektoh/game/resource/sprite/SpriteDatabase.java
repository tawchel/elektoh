package elektoh.game.resource.sprite;

import java.util.HashMap;
import java.util.Map;

import elektoh.game.utils.Log;
import elektoh.game.utils.geometry.Coord;

public class SpriteDatabase {
    private static Map<String, SpriteSheet> spriteSheets;

    public static void initializeSheets() {
        if (spriteSheets != null) {
            for (SpriteSheet sheet : spriteSheets.values()) {
                sheet.destroy();
            }
        }
        spriteSheets = new HashMap<String, SpriteSheet>();
    }

    public static void addSheet(String name, SpriteSheet sheet) {
        spriteSheets.put(name, sheet);
    }

    public static void addSprite(String name, int updateSpeed, Coord... c) {
        if (c.length == 1) {
            addStillSprite(name, c[0]);
        } else {
            addAnimatedSprite(name, updateSpeed, c);
        }
    }

    private static void addStillSprite(String name, Coord c) {
        if (!canLoad(name)) {
            return;
        }
        String[] stringArray = name.split("/", 2);
        spriteSheets.get(stringArray[0]).addStillSprite(stringArray[1], c);
    }

    private static void addAnimatedSprite(String name, int updateSpeed, Coord... cArray) {
        if (!canLoad(name)) {
            return;
        }
        String[] stringArray = name.split("/", 2);
        spriteSheets.get(stringArray[0]).addAnimatedSprite(stringArray[1], updateSpeed, cArray);
    }

    private static boolean canLoad(String name) {
        String[] stringArray = name.split("/", 2);
        if (stringArray.length == 1) {
            Log.warning("CANNOT LOAD " + name + " BECAUSE IT IS MISSING A SHEET MODIFIER");
            return false;
        }
        if (!spriteSheets.containsKey(stringArray[0])) {
            Log.warning("CANNOT LOAD " + name + " BECAUSE THE SHEET " + stringArray[0] + " DOES NOT EXIST");
            return false;
        }
        return true;
    }

    public static Sprite getSprite(String name) {
        if (!canLoad(name)) {
            return null;
        }
        String[] stringArray = name.split("/", 2);
        return spriteSheets.get(stringArray[0]).getSprite(stringArray[1]);
    }
}
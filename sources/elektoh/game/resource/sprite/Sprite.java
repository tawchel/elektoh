package elektoh.game.resource.sprite;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.resource.Texture;
import elektoh.game.utils.Color;
import elektoh.game.utils.QuadSelector;

public abstract class Sprite {
    public abstract Texture getTexture();

    public abstract QuadSelector getRegion();

    public void render() {
        render(Color.WHITE);
    }

    public void render(Color color) {
        QuadSelector region = getRegion();
        render(color, region.width, region.height);
    }

    public void render(Color color, double forcedWidth, double forcedHeight) {
        Texture texture = getTexture();
        QuadSelector region = getRegion();
        if (forcedWidth < 0) {
            forcedWidth = region.width;
        }
        if (forcedHeight < 0) {
            forcedHeight = region.height;
        }
        if (forcedWidth > region.width) {
            forcedWidth = region.width;
        }
        if (forcedHeight > region.height) {
            forcedHeight = region.height;
        }
        color.bind();
        texture.bind();
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        double relPixW = 1D / texture.getWidth();
        double relPixH = 1D / texture.getHeight();
        double relX = region.x * relPixW;
        double relY = region.y * relPixH;
        double relWidth = forcedWidth * relPixW;
        double relHeight = forcedHeight * relPixH;
        glBegin(GL_QUADS);
        glTexCoord2d(relX, relY);
        glVertex2d(0, 0);
        glTexCoord2d(relX + relWidth, relY);
        glVertex2d(forcedWidth, 0);
        glTexCoord2d(relX + relWidth, relY + relHeight);
        glVertex2d(forcedWidth, forcedHeight);
        glTexCoord2d(relX, relY + relHeight);
        glVertex2d(0, forcedHeight);
        glEnd();
    }
}
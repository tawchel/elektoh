package elektoh.game.resource.sprite.load;

import elektoh.game.resource.Texture;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.resource.sprite.SpriteSheet;
import elektoh.game.utils.Direction;
import elektoh.game.utils.Directories;
import elektoh.game.utils.geometry.Coord;

public class LoadUI {
    public static void loadSprites(String id) {
        Texture menu = new Texture(Directories.textures(id) + "/gui/menu.png");
        SpriteDatabase.addSheet("gui", new SpriteSheet(menu, 0, 0, 12, 12));
        loadButtonSet("gui/button.normal", 0, new Coord(1, 1));
        loadButtonSet("gui/button.hover", 0, new Coord(4, 1));
        loadButtonSet("gui/button.clicked", 0, new Coord(7, 1));
        loadButtonSet("gui/button.disabled", 0, new Coord(10, 1));
        loadButtonSet("gui/plate.normal", 0, new Coord(13, 1));
        loadButtonSet("gui/plate.hover", 0, new Coord(16, 1));
        loadButtonSet("gui/plate.clicked", 0, new Coord(19, 1));
        loadButtonSet("gui/plate.disabled", 0, new Coord(13, 1));
        loadButtonSet("gui/plate.glow", 0, new Coord(22, 1));
    }

    private static void loadButtonSet(String name, int updateSpeed, Coord... centers) {
        for (int i = 0; i < 8; i++) {
            Direction d = Direction.values()[i];
            SpriteDatabase.addSprite(name + "." + d.abbrv, updateSpeed, Coord.moveArray(d.x, -d.y, centers));
        }
        SpriteDatabase.addSprite(name + ".C", updateSpeed, centers);
    }
}
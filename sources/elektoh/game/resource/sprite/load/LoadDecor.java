package elektoh.game.resource.sprite.load;

import elektoh.game.resource.Texture;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.resource.sprite.SpriteSheet;
import elektoh.game.utils.Directories;
import elektoh.game.utils.geometry.Coord;

public class LoadDecor {
    public static void loadSprites(String id) {
        Texture decor = new Texture(Directories.textures(id) + "/entity/decor.png");
        SpriteDatabase.addSheet("decor", new SpriteSheet(decor, 0, 0, 25, 34));
        SpriteDatabase.addSprite("decor/cactus.0", 0, new Coord(0, 0));
        SpriteDatabase.addSprite("decor/cactus.1", 0, new Coord(1, 0));
        SpriteDatabase.addSprite("decor/cactus.2", 0, new Coord(2, 0));
    }
}
package elektoh.game.resource.sprite.load;

import elektoh.game.render.RenderEngine;
import elektoh.game.resource.Texture;
import elektoh.game.resource.sprite.SpriteAnimated;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.resource.sprite.SpriteSheet;
import elektoh.game.utils.Directories;
import elektoh.game.utils.geometry.Coord;

public class LoadTile {
    public static void loadSprites(String id) {
        Texture tiles = new Texture(Directories.textures(id) + "/environment/tiles.png");
        SpriteDatabase.addSheet("tiles", new SpriteSheet(tiles, 0, 0, RenderEngine.XY_TILE_WIDTH, RenderEngine.XY_TILE_HEIGHT));
        Coord[] waterAnimation = SpriteAnimated.animationBuilderX(0, 0, 1, 2, 1);
        SpriteDatabase.addSprite("tiles/ocean", 22, waterAnimation);
        SpriteDatabase.addSprite("tiles/grass", 0, new Coord(3, 0));
        SpriteDatabase.addSprite("tiles/sand", 0, new Coord(4, 0));
        SpriteDatabase.addSprite("tiles/stone", 0, new Coord(5, 0));
        loadCTTile("tiles/ocean", 22, Coord.moveArray(0, 1, waterAnimation));
        loadCTTile("tiles/sand", 0, new Coord(4, 1));
    }

    private static void loadCTTile(String name, int updateSpeed, Coord... start) {
        SpriteDatabase.addSprite(name + ".CT.N", updateSpeed, Coord.moveArray(0, 0, start));
        SpriteDatabase.addSprite(name + ".CT.E", updateSpeed, Coord.moveArray(0, 1, start));
        SpriteDatabase.addSprite(name + ".CT.S", updateSpeed, Coord.moveArray(0, 2, start));
        SpriteDatabase.addSprite(name + ".CT.W", updateSpeed, Coord.moveArray(0, 3, start));
        SpriteDatabase.addSprite(name + ".CT.N&E", updateSpeed, Coord.moveArray(0, 4, start));
        SpriteDatabase.addSprite(name + ".CT.E&S", updateSpeed, Coord.moveArray(0, 5, start));
        SpriteDatabase.addSprite(name + ".CT.S&W", updateSpeed, Coord.moveArray(0, 6, start));
        SpriteDatabase.addSprite(name + ".CT.W&N", updateSpeed, Coord.moveArray(0, 7, start));
        SpriteDatabase.addSprite(name + ".CT.N&E&S", updateSpeed, Coord.moveArray(0, 8, start));
        SpriteDatabase.addSprite(name + ".CT.E&S&W", updateSpeed, Coord.moveArray(0, 9, start));
        SpriteDatabase.addSprite(name + ".CT.S&W&N", updateSpeed, Coord.moveArray(0, 10, start));
        SpriteDatabase.addSprite(name + ".CT.W&N&E", updateSpeed, Coord.moveArray(0, 11, start));
        SpriteDatabase.addSprite(name + ".CT.E&S&W&N&E&S", updateSpeed, Coord.moveArray(0, 12, start));
        SpriteDatabase.addSprite(name + ".CT.NE", updateSpeed, Coord.moveArray(0, 13, start));
        SpriteDatabase.addSprite(name + ".CT.SE", updateSpeed, Coord.moveArray(0, 14, start));
        SpriteDatabase.addSprite(name + ".CT.SW", updateSpeed, Coord.moveArray(0, 15, start));
        SpriteDatabase.addSprite(name + ".CT.NW", updateSpeed, Coord.moveArray(0, 16, start));
    }
}

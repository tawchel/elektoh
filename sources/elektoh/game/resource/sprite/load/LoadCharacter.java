package elektoh.game.resource.sprite.load;

import elektoh.game.resource.Texture;
import elektoh.game.resource.sprite.SpriteAnimated;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.resource.sprite.SpriteSheet;
import elektoh.game.utils.Direction;
import elektoh.game.utils.Directories;
import elektoh.game.utils.geometry.Coord;

public class LoadCharacter {
    public static void loadSprites(String id) {
        loadCharacter(id, "human");
    }

    private static void loadCharacter(String id, String name) {
        Texture character = new Texture(Directories.textures(id) + "/entity/char/" + name + ".png");
        SpriteDatabase.addSheet(name, new SpriteSheet(character, 0, 0, 25, 34));
        loadCharSprites(Direction.SOUTHEAST, name, 0);
        loadCharSprites(Direction.EAST, name, 1);
        loadCharSprites(Direction.NORTHEAST, name, 2);
        loadCharSprites(Direction.NORTH, name, 3);
        loadCharSprites(Direction.NORTHWEST, name, 4);
        loadCharSprites(Direction.WEST, name, 5);
        loadCharSprites(Direction.SOUTHWEST, name, 6);
        loadCharSprites(Direction.SOUTH, name, 7);
    }

    private static void loadCharSprites(Direction d, String name, int y) {
        SpriteDatabase.addSprite(name + "/idle." + d.abbrv, 0, new Coord(0, y));
        SpriteDatabase.addSprite(name + "/sneak." + d.abbrv, 20, SpriteAnimated.animationBuilderX(y, 1, 3, 4, 6));
        SpriteDatabase.addSprite(name + "/walk." + d.abbrv, 10, SpriteAnimated.animationBuilderX(y, 1, 2, 3, 4, 5, 6));
        SpriteDatabase.addSprite(name + "/run." + d.abbrv, 7, SpriteAnimated.animationBuilderX(y, 1, 2, 3, 4, 5, 6));
    }
}
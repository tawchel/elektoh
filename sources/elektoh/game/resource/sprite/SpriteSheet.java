package elektoh.game.resource.sprite;

import java.util.HashMap;
import java.util.Map;

import elektoh.game.resource.Texture;
import elektoh.game.utils.QuadSelector;
import elektoh.game.utils.geometry.Coord;

public class SpriteSheet {
    public final Texture texture;
    public final int offsetX;
    public final int offsetY;
    public final int spriteWidth;
    public final int spriteHeight;
    private final Map<String, Sprite> sprites;

    public SpriteSheet(Texture texture, int offsetX, int offsetY, int spriteWidth, int spriteHeight) {
        this.texture = texture;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.spriteWidth = spriteWidth;
        this.spriteHeight = spriteHeight;
        sprites = new HashMap<String, Sprite>();
    }

    public void addStillSprite(String name, Coord c) {
        sprites.put(name, new SpriteStill(texture, getRegion(c)));
    }

    public void addAnimatedSprite(String name, int updateSpeed, Coord[] cArray) {
        QuadSelector[] regions = new QuadSelector[cArray.length];
        for (int i = 0; i < regions.length; i++) {
            regions[i] = getRegion(cArray[i]);
        }
        sprites.put(name, new SpriteAnimated(texture, updateSpeed, regions));
    }

    private QuadSelector getRegion(Coord c) {
        return new QuadSelector((c.x * spriteWidth) + offsetX, (c.y * spriteHeight) + offsetY, spriteWidth, spriteHeight);
    }

    public Sprite getSprite(String name) {
        return sprites.get(name);
    }

    public void destroy() {
        texture.finalize();
    }
}
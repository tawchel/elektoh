package elektoh.game.settings;

import java.io.File;
import java.io.IOException;

import elektoh.game.Elektoh;
import elektoh.game.utils.Directories;
import elektoh.game.utils.GsonHelper;
import elektoh.game.utils.Log;

public class SettingsHandler {
    private static final File file = new File(Directories.DIRECTORY + "settings.cfg");
    public static Settings settings;

    public static void initialize() {
        if (!file.exists()) {
            Log.info("Loading settings, if the settings file is already created this is a BUG. Please report!!");
            makeFile();
            settings = new Settings();
            save();
        } else {
            load();
        }
    }

    private static void makeFile() {
        try {
            file.createNewFile();
        } catch (IOException e) {
            Log.severe("FAILED TO CREATE SETTINGS.CFG, Because:");
            e.printStackTrace();
            Elektoh.exit();
        }
    }

    public static void save() {
        try {
            GsonHelper.save(file, settings);
        } catch (Exception e) {
            Log.warning("FAILED TO SAVE SETTINGS.CFG, Because:");
            e.printStackTrace();
            file.delete();
        }
    }

    public static void load() {
        try {
            settings = GsonHelper.load(file, Settings.class);
        } catch (Exception e) {
            Log.severe("FAILED TO LOAD SETTINGS.CFG, Because:");
            e.printStackTrace();
        }
    }
}
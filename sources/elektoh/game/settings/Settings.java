package elektoh.game.settings;

import com.google.gson.annotations.SerializedName;

public class Settings {
    @SerializedName("First Launch")
    public boolean first_launch = true;
    public String language = "en_US";
    @SerializedName("Input Type")
    public int input = 0;
    @SerializedName("Key Bindings")
    public SettingsKeyBinds keyBinds = new SettingsKeyBinds();
}
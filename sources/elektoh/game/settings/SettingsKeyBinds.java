package elektoh.game.settings;

import org.lwjgl.input.Keyboard;

import com.google.gson.annotations.SerializedName;

public class SettingsKeyBinds {
    @SerializedName("Mouse Movement Button")
    public int mouseMovement = 1;
    public int upKey = Keyboard.KEY_W;
    public int downKey = Keyboard.KEY_S;
    public int leftKey = Keyboard.KEY_A;
    public int rightKey = Keyboard.KEY_D;
    public int sneakKey = Keyboard.KEY_LSHIFT;
    public int reloadResourcesKey = Keyboard.KEY_F1;
    public int crudeSave = Keyboard.KEY_F5;
}
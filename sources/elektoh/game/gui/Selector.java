package elektoh.game.gui;

public class Selector {
    private static ISelectCandidate selected;
    private static boolean unselect = true;

    public static void select(ISelectCandidate candidate) {
        selected = candidate;
    }

    public static void unselect() {
        if (unselect) {
            selected = null;
        }
    }

    public static boolean isSelected(ISelectCandidate candidate) {
        return candidate == selected;
    }

    public static ISelectCandidate getSelected() {
        return selected;
    }

    public static void disableUnselect() {
        unselect = false;
    }

    public static void enableUnselect() {
        unselect = true;
    }
}
package elektoh.game.gui;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

public class GuiElementList {
    private final List<GuiElement> elements;

    public GuiElementList() {
        elements = new ArrayList<GuiElement>();
    }

    public void add(GuiElement element) {
        elements.add(element);
    }

    public boolean getButtonClicked(String buttonName) {
        return getButtonClicked(buttonName, false);
    }

    public boolean getButtonClicked(String buttonName, boolean override) {
        for (GuiElement element : elements) {
            if (element instanceof Button) {
                Button button = (Button) element;
                if (button.name.equals(buttonName) && button.isClicked(override)) {
                    return true;
                }
            }
        }
        return false;
    }

    public GuiElement getElement(String elementName) {
        for (GuiElement element : elements) {
            if (elementName.equals(element.name)) {
                return element;
            }
        }
        return null;
    }

    public void update(int mouseX, int mouseY) {
        for (GuiElement element : elements) {
            element.update(mouseX, mouseY);
        }
    }

    public void render() {
        glPushMatrix();
        for (GuiElement element : elements) {
            element.render();
        }
        glPopMatrix();
    }
}
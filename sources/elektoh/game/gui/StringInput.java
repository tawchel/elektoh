package elektoh.game.gui;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.io.types.KeyboardInput;
import elektoh.game.render.Text;
import elektoh.game.utils.Align;
import elektoh.game.utils.Color;
import elektoh.game.utils.Direction;
import elektoh.game.utils.QuadSelector;

public class StringInput extends GuiElement implements ISelectCandidate {
    private String string = "";
    private final int maxCharacters;

    public StringInput(String name, String defaultString, boolean active, int maxCharacters, QuadSelector region) {
        super(name, "plate", region);
        string = defaultString;
        this.maxCharacters = maxCharacters;
        Selector.select(this);
    }

    @Override
    public void update(int mouseX, int mouseY) {
        super.update(mouseX, mouseY);
        if (click) {
            Selector.select(this);
        }
        if (Selector.isSelected(this)) {
            String tempString = KeyboardInput.getStringFromTyping(string);
            if (!(tempString.length() > maxCharacters)) {
                string = tempString;
            }
        }
    }

    @Override
    public void render() {
        glPushMatrix();
        glTranslated(region.x, region.y, 0);
        if (Selector.isSelected(this)) {
            for (Direction d : Direction.values()) {
                glPushMatrix();
                glTranslated(d.x * 1, d.y * 2, 0);
                doRender("glow", Color.Swatches.GLOW);
                glPopMatrix();
            }
        }
        super.render();
        renderString();
        glPopMatrix();
    }

    private void renderString() {
        String stringToRender = string;
        if (Selector.isSelected(this)) {
            stringToRender += KeyboardInput.blinky();
        }
        Text text = new Text();
        text.setTextColor(Color.WHITE);
        text.setShadowColor(Color.BLACK);
        text.setAlign(Align.LEFT, Align.CENTER);
        text.draw(stringToRender, region.blankTranslate().trim(10, 0));
    }

    public String getOutput() {
        return string;
    }
}
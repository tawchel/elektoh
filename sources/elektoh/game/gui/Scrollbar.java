package elektoh.game.gui;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

import elektoh.game.io.types.MouseInput;
import elektoh.game.utils.MathUtils;
import elektoh.game.utils.QuadSelector;
import elektoh.game.utils.geometry.Point;

public class Scrollbar extends GuiElement {
    public final List<GuiElement> elements;
    public final Grid grid;
    private double scroll = 0;

    public Scrollbar(String name, QuadSelector region) {
        super(name, null, region);
        elements = new ArrayList<GuiElement>();
        grid = new Grid((int) region.width, (int) region.height, 6, 4, 0, 0, 4);
    }

    @Override
    public void update(int mouseX, int mouseY) {
        scroll(MouseInput.scrollSpeed);
        for (GuiElement element : elements) {
            QuadSelector r = element.region;
            r = r.translate(region.x, region.y - scroll);
            Point p1 = new Point(r.x, r.y);
            Point p2 = new Point(r.x + r.width, r.y);
            Point p3 = new Point(r.x + r.width, r.y + r.height);
            Point p4 = new Point(r.x, r.y + r.height);
            element.disabled = (region.outOfBounds(p1) || region.outOfBounds(p2) || region.outOfBounds(p3) || region.outOfBounds(p4));
            element.update((int) (mouseX - region.x), (int) ((mouseY - region.y) + scroll));
        }
    }

    public double getMaxScroll() {
        double maxScroll = 0;
        for (GuiElement element : elements) {
            double d1 = element.region.y / 1.25d;
            if (d1 > maxScroll) {
                maxScroll = d1;
            }
        }
        return maxScroll;
    }

    public void scroll(double d) {
        scroll = MathUtils.minMax(scroll + d, 0, getMaxScroll());
    }

    public double getScroll() {
        return scroll;
    }

    public void add(GuiElement element) {
        elements.add(element);
    }

    public GuiElement getElement(String elementName) {
        for (GuiElement element : elements) {
            if (elementName.equals(element.name)) {
                return element;
            }
        }
        return null;
    }

    @Override
    public void render() {
        glPushMatrix();
        glTranslated(region.x, region.y, 0);
        glTranslated(0, -scroll, 0);
        for (GuiElement element : elements) {
            element.render();
        }
        glPopMatrix();
    }
}

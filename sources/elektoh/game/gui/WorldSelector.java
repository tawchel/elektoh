package elektoh.game.gui;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.render.Text;
import elektoh.game.utils.Align;
import elektoh.game.utils.Color;
import elektoh.game.utils.Direction;
import elektoh.game.utils.QuadSelector;

public class WorldSelector extends GuiElement implements ISelectCandidate {
    public WorldSelector(String name, Grid grid, int index) {
        super(name, "plate", getRegion(grid, index));
    }

    private static QuadSelector getRegion(Grid grid, int index) {
        int rowIndex = (int) Math.floor(index / 2d);
        boolean even = (index % 2) == 0;
        return grid.getRegion(even ? 1 : 4, (rowIndex * 2) + 1, 3, 2);
    }

    @Override
    public void update(int mouseX, int mouseY) {
        super.update(mouseX, mouseY);
        if (click) {
            Selector.select(this);
        }
    }

    @Override
    public void render() {
        glPushMatrix();
        glTranslated(region.x, region.y, 0);
        if (Selector.isSelected(this)) {
            for (Direction d : Direction.values()) {
                glPushMatrix();
                glTranslated(d.x * 1, d.y * 2, 0);
                doRender("glow", Color.Swatches.GLOW);
                glPopMatrix();
            }
        }
        super.render();
        renderName();
        glPopMatrix();
    }

    private void renderName() {
        Text text = new Text();
        text.setTextColor(Color.WHITE);
        text.setShadowColor(Color.BLACK);
        text.setAlign(Align.RIGHT, Align.TOP);
        text.draw(name, new QuadSelector(0, 2, region.width, 8).trim(2, 0));
    }
}
package elektoh.game.gui;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.Elektoh;
import elektoh.game.io.types.MouseInput;
import elektoh.game.resource.sprite.Sprite;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.utils.Color;
import elektoh.game.utils.QuadSelector;

public abstract class GuiElement {
    public final String name;
    public final QuadSelector region;
    public final String resource;
    protected boolean hover = false;
    protected boolean click = false;
    protected int clickRender = 0;
    public boolean disabled = false;

    public GuiElement(String name, String resource, QuadSelector region) {
        this.name = name;
        this.region = region;
        this.resource = resource;
    }

    public void update(int mouseX, int mouseY) {
        if (clickRender != 0) {
            clickRender--;
        }
        boolean flag = false;
        if ((mouseX >= region.x) && (mouseX <= (region.x + region.width))) {
            if ((mouseY >= region.y) && (mouseY <= (region.y + region.height))) {
                flag = true;
            }
        }
        if (!MouseInput.mouseLeftClick_single || !flag) {
            click = false;
        }
        if (flag && MouseInput.mouseLeftClick_single) {
            click = true;
        }
        hover = flag;
        if (click) {
            clickRender = 8;
        }
        if (disabled) {
            hover = false;
        }
    }

    public void render() {
        doRender(null, Color.WHITE);
        if (Elektoh.DEV_ART) {
            renderHitbox(Color.RED);
        }
    }

    protected void doRender(String customSuffix, Color color) {
        double relW = (region.width / 12D) - 2;
        double relH = (region.height / 12D) - 2;
        double decW = relW - Math.floor(relW);
        double decH = relH - Math.floor(relH);
        int maxW = (int) (Math.ceil(relW) + 3);
        int maxH = (int) (Math.ceil(relH) + 3);
        int untranslateX = 0;
        for (int itrX = 0; itrX < maxW; itrX++) {
            int buttonWidth = 12;
            if (itrX == 1) {
                buttonWidth = (int) (12 * (decW / 2));
            }
            if (itrX == (maxW - 2)) {
                buttonWidth = (int) (12 * (decW / 2));
            }
            int untranslateY = 0;
            for (int itrY = 0; itrY < maxH; itrY++) {
                int buttonHeight = 12;
                if (itrY == 1) {
                    buttonHeight = (int) (12 * (decH / 2));
                }
                if (itrY == (maxH - 2)) {
                    buttonHeight = (int) (12 * (decH / 2));
                }
                Sprite sprite = getButtonSprite(itrX, itrY, maxW, maxH, customSuffix);
                if (sprite != null) {
                    sprite.render(color, buttonWidth, buttonHeight);
                }
                glTranslated(0, buttonHeight, 0);
                untranslateY += buttonHeight;
            }
            glTranslated(0, -untranslateY, 0);
            glTranslated(buttonWidth, 0, 0);
            untranslateX += buttonWidth;
        }
        glTranslated(-untranslateX, 0, 0);
    }

    private Sprite getButtonSprite(int itrX, int itrY, int maxW, int maxH, String customSuffix) {
        String type = "normal";
        String d = "C";
        if (itrY == 0) {
            d = "N";
        }
        if (itrX == (maxW - 1)) {
            d = "E";
        }
        if (itrY == (maxH - 1)) {
            d = "S";
        }
        if (itrX == 0) {
            d = "W";
        }
        if ((itrX == 0) && (itrY == 0)) {
            d = "NW";
        }
        if ((itrX == (maxW - 1)) && (itrY == 0)) {
            d = "NE";
        }
        if ((itrX == (maxW - 1)) && (itrY == (maxH - 1))) {
            d = "SE";
        }
        if ((itrX == 0) && (itrY == (maxH - 1))) {
            d = "SW";
        }
        if (hover) {
            type = "hover";
        }
        if (clickRender > 0) {
            type = "clicked";
        }
        if (disabled) {
            type = "disabled";
        }
        if (customSuffix != null) {
            type = customSuffix;
        }
        return SpriteDatabase.getSprite("gui/" + resource + "." + type + "." + d);
    }

    protected void renderHitbox(Color color) {
        glDisable(GL_TEXTURE_2D);
        glPushMatrix();
        color.bind();
        glBegin(GL_QUADS);
        {
            glVertex2d(0, 0);
            glVertex2d(region.width, 0);
            glVertex2d(region.width, region.height);
            glVertex2d(0, region.height);
        }
        glEnd();
        glPopMatrix();
        glEnable(GL_TEXTURE_2D);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [name=" + name + ", region=" + region + ", hover=" + hover + ", disabled=" + disabled + "]";
    }
}

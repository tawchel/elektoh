package elektoh.game.gui;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.Elektoh;
import elektoh.game.utils.Color;
import elektoh.game.utils.QuadSelector;

public class Grid {
    public final int width;
    public final int height;
    public final int columns;
    public final int rows;
    public final double columnWidth;
    public final double rowHeight;
    public final int translateX;
    public final int translateY;
    public final int margin;

    public Grid(int columns, int rows, int margin) {
        this(Elektoh.WINDOW_DEF_X / 4, Elektoh.WINDOW_DEF_Y / 4, columns, rows, 0, 0, margin);
    }

    public Grid(int width, int height, int columns, int rows, int translateX, int translateY, int margin) {
        this.width = width;
        this.height = height;
        this.columns = columns;
        this.rows = rows;
        this.translateX = translateX;
        this.translateY = translateY;
        this.margin = margin;
        columnWidth = (double) width / (double) columns;
        rowHeight = (double) height / (double) rows;
    }

    public QuadSelector getRegion(double gridX, double gridY, double gridSizeX, double gridSizeY) {
        return getRegion(gridX, gridY, gridSizeX, gridSizeY, margin);
    }

    public QuadSelector getRegion(double gridX, double gridY, double gridSizeX, double gridSizeY, double margin) {
        gridX -= 1;
        gridY -= 1;
        double x = ((gridX * columnWidth) + translateX) + margin;
        double y = ((gridY * rowHeight) + translateY) + margin;
        double width = (columnWidth * gridSizeX) - (margin * 2);
        double height = (rowHeight * gridSizeY) - (margin * 2);
        QuadSelector region = new QuadSelector(x, y, width, height);
        return region;
    }

    public void render() {
        glDisable(GL_TEXTURE_2D);
        glLineWidth(4);
        glPushMatrix();
        Color.BLACK.bind();
        for (int i = 0; i < (columns + 1); i++) {
            drawVertical(translateX + (int) Math.round(columnWidth * i));
        }
        for (int i = 0; i < (rows + 1); i++) {
            drawHorizontal(translateY + (int) Math.round(rowHeight * i));
        }
        glPopMatrix();
        glLineWidth(1);
        glEnable(GL_TEXTURE_2D);
    }

    private void drawVertical(int x) {
        glBegin(GL_LINES);
        glVertex3d(x, translateY, 0);
        glVertex3d(x, translateY + height, 0);
        glEnd();
    }

    private void drawHorizontal(int y) {
        glBegin(GL_LINES);
        glVertex3d(translateX, y, 0);
        glVertex3d(translateX + width, y, 0);
        glEnd();
    }
}
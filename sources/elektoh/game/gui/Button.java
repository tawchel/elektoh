package elektoh.game.gui;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.render.Text;
import elektoh.game.resource.Localizer;
import elektoh.game.utils.Color;
import elektoh.game.utils.QuadSelector;

public class Button extends GuiElement {
    public Button(String name, QuadSelector region) {
        this(name, region, false);
    }

    public Button(String name, QuadSelector region, boolean disabled) {
        super(name, "button", region);
        this.disabled = disabled;
    }

    @Override
    public void render() {
        glPushMatrix();
        glTranslated(region.x, region.y, 0);
        super.render();
        renderName();
        glPopMatrix();
    }

    private void renderName() {
        Color textColor = Color.WHITE;
        if (disabled) {
            textColor = Color.greyScale(0.65d);
        }
        String localizedName = Localizer.localize("button." + name);
        Text text = new Text();
        text.setTextColor(textColor);
        text.setShadowColor(Color.BLACK);
        text.draw(localizedName, region.blankTranslate());
    }

    public boolean isClicked() {
        return isClicked(false);
    }

    public boolean isClicked(boolean override) {
        if (disabled && !override) {
            return false;
        }
        return clickRender == 1;
    }

    public boolean hover() {
        return hover;
    }

    @Override
    public String toString() {
        return "Button [name=" + name + ", hover=" + hover + ", disabled=" + disabled + "]";
    }
}
package elektoh.game.utils;

import static elektoh.game.io.types.KeyboardInput.isKeyDown;
import static elektoh.game.settings.SettingsHandler.settings;

public enum Direction {
    NORTH("N", 90, 0, 1, true),
    NORTHEAST("NE", 45, 1, 1, false),
    EAST("E", 0, 1, 0, true),
    SOUTHEAST("SE", 315, 1, -1, false),
    SOUTH("S", 270, 0, -1, true),
    SOUTHWEST("SW", 225, -1, -1, false),
    WEST("W", 180, -1, 0, true),
    NORTHWEST("NW", 135, -1, 1, false);
    public static final Direction[] PRIMARY = new Direction[] { NORTH, EAST, SOUTH, WEST };
    public static final Direction[] SECONDARY = new Direction[] { NORTHEAST, SOUTHEAST, SOUTHWEST, NORTHWEST };
    public final String abbrv;
    public final double angle;
    public final int x;
    public final int y;
    public final boolean primary;

    Direction(String abbrv, double angle, int x, int y, boolean primary) {
        this.abbrv = abbrv;
        this.angle = Math.toRadians(angle);
        this.x = x;
        this.y = y;
        this.primary = primary;
    }

    public int iso_x() {
        return -x;
    }

    public int iso_y() {
        return y;
    }

    public Direction xyToIso() {
        switch (this) {
            case NORTH:
                return NORTHWEST;
            case NORTHEAST:
                return NORTH;
            case EAST:
                return NORTHEAST;
            case SOUTHEAST:
                return EAST;
            case SOUTH:
                return SOUTHEAST;
            case SOUTHWEST:
                return SOUTH;
            case WEST:
                return SOUTHWEST;
            case NORTHWEST:
                return WEST;
        }
        return null;
    }

    public static Direction getDirectionFromRotation(double rotation) {
        if ((rotation >= helper(15)) || (rotation < helper(1))) {
            return EAST;
        }
        if ((rotation >= helper(1)) && (rotation < helper(3))) {
            return SOUTHEAST;
        }
        if ((rotation >= helper(3)) && (rotation < helper(5))) {
            return SOUTH;
        }
        if ((rotation >= helper(5)) && (rotation < helper(7))) {
            return SOUTHWEST;
        }
        if ((rotation >= helper(7)) && (rotation < helper(9))) {
            return WEST;
        }
        if ((rotation >= helper(9)) && (rotation < helper(11))) {
            return NORTHWEST;
        }
        if ((rotation >= helper(11)) && (rotation < helper(13))) {
            return NORTH;
        }
        if ((rotation >= helper(13)) && (rotation < helper(15))) {
            return NORTHEAST;
        }
        return NORTH;
    }

    private static double helper(int a) {
        return (a / 16d) * (2 * Math.PI);
    }

    public Direction opposite() {
        switch (this) {
            case NORTH:
                return SOUTH;
            case NORTHEAST:
                return SOUTHWEST;
            case EAST:
                return WEST;
            case SOUTHEAST:
                return NORTHWEST;
            case SOUTH:
                return NORTH;
            case SOUTHWEST:
                return NORTHEAST;
            case WEST:
                return EAST;
            case NORTHWEST:
                return SOUTHEAST;
        }
        return null;
    }

    public static Direction getDirectionFromKeys() {
        if (isKeyDown(settings.keyBinds.upKey)) {
            if (isKeyDown(settings.keyBinds.leftKey)) {
                return NORTHWEST;
            }
            if (isKeyDown(settings.keyBinds.rightKey)) {
                return NORTHEAST;
            }
            return NORTH;
        }
        if (isKeyDown(settings.keyBinds.downKey)) {
            if (isKeyDown(settings.keyBinds.leftKey)) {
                return SOUTHWEST;
            }
            if (isKeyDown(settings.keyBinds.rightKey)) {
                return SOUTHEAST;
            }
            return SOUTH;
        }
        if (isKeyDown(settings.keyBinds.leftKey)) {
            return WEST;
        }
        if (isKeyDown(settings.keyBinds.rightKey)) {
            return EAST;
        }
        return null;
    }
}
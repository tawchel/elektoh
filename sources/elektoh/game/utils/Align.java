package elektoh.game.utils;

public enum Align {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM,
    CENTER;
}
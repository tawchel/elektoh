package elektoh.game.utils.noise;

import java.util.Random;

public class SmoothNoise2D extends Noise2D {
    int detail;

    public SmoothNoise2D(int size, long seed, int detail) {
        super(size, seed);
        if (detail >= size) {
            detail = size;
        }
        this.detail = detail;
    }

    @Override
    public SmoothNoise2D generateNoise() {
        Random rand = new Random(seed);
        float[][] generatedIntervals = new float[(size / detail) + 2][(size / detail) + 2];
        for (int i = 0; i < generatedIntervals.length; i++) {
            for (int j = 0; j < generatedIntervals[i].length; j++) {
                generatedIntervals[i][j] = getRand(rand, i, generatedIntervals.length, j, generatedIntervals[i].length);
            }
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                // @formatter:off
                setNoiseValue(i, j, cosineInterpolate(
                                                      ((float) i % detail) / detail,
                                                      ((float) j % detail) / detail,
                                                      generatedIntervals[i / detail][j / detail],
                                                      generatedIntervals[(i / detail) + 1][j / detail],
                                                      generatedIntervals[(i / detail)][(j / detail) + 1],
                                                      generatedIntervals[(i / detail) + 1][(j / detail) + 1]));
                // @formatter:on
            }
        }
        return this;
    }

    public float getRand(Random rand, int i, int length, int j, int length2) {
        return rand.nextFloat();
    }

    private float cosineInterpolate(float xRatio, float yRatio, float a, float b, float c, float d) {
        float xInt = cosineInterpolate(xRatio, a, b);
        float yInt = cosineInterpolate(xRatio, c, d);
        return cosineInterpolate(yRatio, xInt, yInt);
    }

    private float cosineInterpolate(float ratio, float a, float b) {
        float temp = (1 - (float) Math.cos(ratio * Math.PI)) / 2f;
        return ((a * (1 - temp)) + (b * temp));
    }
}
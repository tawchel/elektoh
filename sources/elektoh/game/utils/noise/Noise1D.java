package elektoh.game.utils.noise;

import java.util.Random;

public class Noise1D {
    protected int size;
    protected long seed;
    protected float[] noise;
    private float lowestValue = 1;
    private float highestValue = 0;
    private float averageCount;

    public Noise1D(int size, long seed) {
        this.size = size;
        this.seed = seed;
        noise = new float[size];
    }

    public Noise1D generateNoise() {
        Random rand = new Random(seed);
        for (int i = 0; i < size; i++) {
            setNoiseValue(i, rand.nextFloat());
        }
        return this;
    }

    public void setNoiseValue(int i, float value) {
        noise[i] = value;
        if (value < lowestValue) {
            lowestValue = value;
        }
        if (value > highestValue) {
            highestValue = value;
        }
        averageCount += value;
    }

    public float getAverage() {
        return contrast(averageCount / (size * size));
    }

    public int getSize() {
        return size;
    }

    public float[] getNoise() {
        return noise;
    }

    public float getValue(int i) {
        return contrast(noise[i]);
    }

    private float contrast(float f) {
        float slope = 1 / (highestValue - lowestValue);
        float yIntercept = -(lowestValue * slope);
        return (slope * f) + yIntercept;
    }
}
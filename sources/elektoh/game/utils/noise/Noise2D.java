package elektoh.game.utils.noise;

import java.util.Random;

public class Noise2D {
    protected int size;
    protected long seed;
    protected float[][] noise;
    private float lowestValue = 1;
    private float highestValue = 0;
    private float averageCount;

    public Noise2D(int size, long seed) {
        this.size = size;
        this.seed = seed;
        noise = new float[size][size];
    }

    public Noise2D generateNoise() {
        Random rand = new Random(seed);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                noise[i][j] = rand.nextFloat();
            }
        }
        return this;
    }

    public void setNoiseValue(int i, int j, float value) {
        noise[i][j] = value;
        if (value < lowestValue) {
            lowestValue = value;
        }
        if (value > highestValue) {
            highestValue = value;
        }
        averageCount += value;
    }

    public float getAverage() {
        return contrast(averageCount / (size * size));
    }

    public int getSize() {
        return size;
    }

    public float[][] getNoise() {
        return noise;
    }

    public float getValueAtPoint(double x, double y) {
        if ((x < 0) || (y < 0)) {
            return 0;
        }
        if ((x > 1) || (y > 1)) {
            return 0;
        }
        return contrast(noise[(int) Math.round(x * (size - 1))][(int) Math.round(y * (size - 1))]);
    }

    private float contrast(float f) {
        float slope = 1 / (highestValue - lowestValue);
        float yIntercept = -(lowestValue * slope);
        return (slope * f) + yIntercept;
    }
}
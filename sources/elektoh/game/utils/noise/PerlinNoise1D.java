package elektoh.game.utils.noise;

import java.util.ArrayList;

public class PerlinNoise1D extends Noise1D {
    public int[] octaves;

    public PerlinNoise1D(int size, long seed, int[] octaves) {
        super(size, seed);
        this.octaves = octaves;
    }

    @Override
    public PerlinNoise1D generateNoise() {
        ArrayList<Noise1D> components = new ArrayList<Noise1D>();
        for (int i = 0; i < octaves.length; i++) {
            components.add(getSmoothNoise1D(size, seed + i, octaves[i]).generateNoise());
        }
        for (int i = 0; i < size; i++) {
            float curNoise = 0;
            for (int k = 0; k < components.size(); k++) {
                curNoise += components.get(k).noise[i] * Math.pow(octaves.length - k, .7);
            }
            setNoiseValue(i, curNoise / (octaves.length * octaves.length));
        }
        return this;
    }

    public SmoothNoise1D getSmoothNoise1D(int size, long seed, int detail) {
        return new SmoothNoise1D(size, seed, detail);
    }
}
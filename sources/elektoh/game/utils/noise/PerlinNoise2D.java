package elektoh.game.utils.noise;

import java.util.ArrayList;

public class PerlinNoise2D extends Noise2D {
    public int[] octaves;

    public PerlinNoise2D(int size, long seed, int[] octaves) {
        super(size, seed);
        this.octaves = octaves;
    }

    @Override
    public PerlinNoise2D generateNoise() {
        ArrayList<Noise2D> components = new ArrayList<Noise2D>();
        for (int i = 0; i < octaves.length; i++) {
            components.add(getSmoothNoise2D(size, seed + i, octaves[i]).generateNoise());
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                float curNoise = 0;
                for (int k = 0; k < components.size(); k++) {
                    curNoise += components.get(k).noise[i][j] * Math.pow(octaves.length - k, .7);
                }
                setNoiseValue(i, j, curNoise / (octaves.length * octaves.length));
            }
        }
        return this;
    }

    public SmoothNoise2D getSmoothNoise2D(int size, long seed, int detail) {
        return new SmoothNoise2D(size, seed, detail);
    }
}
package elektoh.game.utils.noise;

import java.util.Random;

public class BiasedSmoothNoise2D extends SmoothNoise2D {
    BiasType type;
    float extraData;

    public BiasedSmoothNoise2D(int size, long seed, int detail, BiasType type, float extraData) {
        super(size, seed, detail);
        this.type = type;
        this.extraData = extraData;
    }

    @Override
    public float getRand(Random rand, int x, int width, int y, int height) {
        return type.getBiasedValue(rand, x, width, y, height, extraData);
    }
}
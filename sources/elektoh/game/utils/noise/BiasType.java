package elektoh.game.utils.noise;

import static java.lang.Math.*;

import java.util.Random;

public enum BiasType {
    CONCENTRATED,
    GRADIENT;
    public float getBiasedValue(Random rand, int x, int width, int y, int height, float extraData) {
        switch (this) {
            case CONCENTRATED:
                return biasCONCENTRATED(rand, x, width, y, height, extraData);
            case GRADIENT:
                return biasGRADIENT(rand, x, width, y, height, extraData);
            default:
                return 0;
        }
    }

    private float biasCONCENTRATED(Random rand, int x, int width, int y, int height, float extraData) {
        float disToCenter = (float) (sqrt(pow((x - (width / 2f)), 2) + pow((y - (width / 2f)), 2)) / (height / 2f)) / extraData;
        disToCenter = disToCenter > 1 ? 0 : 1f - disToCenter;
        return rand.nextFloat() * disToCenter;
    }

    private float biasGRADIENT(Random rand, int x, int width, int y, int height, float extraData) {
        float disToBottom = height - y;
        disToBottom /= height;
        disToBottom = 1f - disToBottom;
        return (rand.nextFloat() + (disToBottom * extraData)) / (1 + extraData);
    }
}
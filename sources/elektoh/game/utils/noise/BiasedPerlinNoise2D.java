package elektoh.game.utils.noise;

public class BiasedPerlinNoise2D extends PerlinNoise2D {
    BiasType type;
    float extraData;

    public BiasedPerlinNoise2D(int size, long seed, int[] octaves, BiasType type, float extraData) {
        super(size, seed, octaves);
        this.type = type;
        this.extraData = extraData;
    }

    @Override
    public SmoothNoise2D getSmoothNoise2D(int size, long seed, int detail) {
        return new BiasedSmoothNoise2D(size, seed, detail, type, extraData);
    }
}
package elektoh.game.utils.noise;

import java.util.Random;

public class SmoothNoise1D extends Noise1D {
    int detail;

    public SmoothNoise1D(int size, long seed, int detail) {
        super(size, seed);
        if (detail >= size) {
            detail = size;
        }
        this.detail = detail;
    }

    @Override
    public SmoothNoise1D generateNoise() {
        Random rand = new Random(seed);
        float[] generatedIntervals = new float[(size / detail) + 2];
        for (int i = 0; i < generatedIntervals.length; i++) {
            generatedIntervals[i] = getRand(rand, i, generatedIntervals.length);
        }
        for (int i = 0; i < size; i++) {
            // @formatter:off
            setNoiseValue(i, cosineInterpolate(
                                               ((float) i % detail) / detail,
                                               generatedIntervals[i / detail],
                                               generatedIntervals[(i / detail) + 1]));
            // @formatter:on
        }
        return this;
    }

    public float getRand(Random rand, int i, int length) {
        return rand.nextFloat();
    }

    private float cosineInterpolate(float ratio, float a, float b) {
        float temp = (1 - (float) Math.cos(ratio * Math.PI)) / 2f;
        return ((a * (1 - temp)) + (b * temp));
    }
}
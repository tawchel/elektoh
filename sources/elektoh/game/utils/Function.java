package elektoh.game.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

// Really a 1-to-1 function
public class Function<A, B> {
    private Map<A, B> aMap;
    private Map<B, A> bMap;

    public Function() {
        aMap = new HashMap<A, B>();
        bMap = new HashMap<B, A>();
    }

    @Meme("http://i.imgur.com/nlArAKx.jpg")
    public boolean put(A a, B b) {
        if (contains(a) || contains(b)) {
            return false;
        }
        aMap.put(a, b);
        bMap.put(b, a);
        return true;
    }

    public B getFromA(A a) {
        return aMap.get(a);
    }

    public A getFromB(B b) {
        return bMap.get(b);
    }

    public boolean contains(Object obj) {
        return aMap.containsKey(obj) || bMap.containsKey(obj);
    }

    public Collection<A> collectionA() {
        return aMap.keySet();
    }

    public Collection<B> collectionB() {
        return bMap.keySet();
    }

    public int size() {
        return aMap.size();
    }
}
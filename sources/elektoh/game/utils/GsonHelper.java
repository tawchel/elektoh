package elektoh.game.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class GsonHelper {
    public static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    public static final JsonParser PARSER = new JsonParser();

    public static void save(File file, Object obj) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(GSON.toJson(obj));
        bw.close();
    }

    public static <T> T load(File file, Class<T> clazz) throws Exception {
        return GSON.fromJson(new FileReader(file), clazz);
    }

    public static JsonArray parseArray(File file) {
        JsonArray root = null;
        try {
            root = PARSER.parse(new FileReader(file)).getAsJsonArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return root;
    }

    public static JsonObject parseObject(File file) {
        JsonObject root = null;
        try {
            root = PARSER.parse(new FileReader(file)).getAsJsonObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return root;
    }
}
package elektoh.game.utils;

import com.google.gson.JsonObject;

import elektoh.game.utils.geometry.Point;

public class DataObject {
    private JsonObject json;

    public DataObject(JsonObject json) {
        this.json = json;
    }

    public void add(String key, Number value) {
        json.addProperty(key, value);
    }

    public void add(String key, String value) {
        json.addProperty(key, value);
    }

    public void add(String key, char value) {
        json.addProperty(key, value);
    }

    public void add(String key, boolean value) {
        json.addProperty(key, value);
    }

    public void add(String key, Point value) {
        add(key + "-x", value.x);
        add(key + "-y", value.y);
    }

    public byte getByte(String key) {
        return json.get(key).getAsByte();
    }

    public short getShort(String key) {
        return json.get(key).getAsShort();
    }

    public int getInteger(String key) {
        return json.get(key).getAsInt();
    }

    public double getDouble(String key) {
        return json.get(key).getAsDouble();
    }

    public float getFloat(String key) {
        return json.get(key).getAsFloat();
    }

    public long getLong(String key) {
        return json.get(key).getAsLong();
    }

    public char getCharacter(String key) {
        return json.get(key).getAsCharacter();
    }

    public String getString(String key) {
        return json.get(key).getAsString();
    }

    public boolean getBoolean(String key) {
        return json.get(key).getAsBoolean();
    }

    public Point getPoint(String key) {
        return new Point(getDouble(key + "-x"), getDouble(key + "-y"));
    }
}
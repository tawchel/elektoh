package elektoh.game.utils;

import elektoh.game.utils.geometry.Edge;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;

public class QuadSelector {
    public final double x;
    public final double y;
    public final double width;
    public final double height;

    public QuadSelector(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public QuadSelector blankTranslate() {
        return new QuadSelector(0, 0, width, height);
    }

    public QuadSelector translate(double x, double y) {
        return new QuadSelector(this.x + x, this.y + y, width, height);
    }

    public QuadSelector trim(double trimX, double trimY) {
        return new QuadSelector(x + trimX, y + trimY, width - (4 * trimX), height - (4 * trimY));
    }

    public boolean outOfBounds(Point p) {
        if ((p.x < x) || (p.x > (x + width))) {
            return true;
        }
        if ((p.y < y) || (p.y > (y + height))) {
            return true;
        }
        return false;
    }

    public Polygon toPolygon() {
        Point p1 = new Point(x, y);
        Point p2 = new Point(x + width, y);
        Point p3 = new Point(x + width, y + height);
        Point p4 = new Point(x, y + height);
        Edge e1 = new Edge(p1, p2);
        Edge e2 = new Edge(p2, p3);
        Edge e3 = new Edge(p3, p4);
        Edge e4 = new Edge(p4, p1);
        return new Polygon(e1, e2, e3, e4);
    }

    @Override
    public String toString() {
        return "QuadSelector [x=" + x + "-->" + (x + width) + ", y=" + y + "-->" + (y + height) + "]";
    }
}
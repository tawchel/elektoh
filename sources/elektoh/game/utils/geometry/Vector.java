package elektoh.game.utils.geometry;

import static java.lang.Math.*;

public class Vector {
    private double deltaX;
    private double deltaY;
    private double magnitude;
    private double angle;

    private Vector(boolean polar, double var1, double var2) {
        if (!polar) {
            deltaX = var1;
            deltaY = var2;
            solvePolar();
        } else {
            magnitude = var1;
            angle = var2;
            solveStandard();
        }
    }

    public static Vector createStandard(Point start, Point finish) {
        return createStandard(finish.x - start.x, finish.y - start.y);
    }

    public static Vector createStandard(double deltaX, double deltaY) {
        return new Vector(false, deltaX, deltaY);
    }

    public static Vector createPolar(double magnitude, double angle) {
        return new Vector(true, magnitude, angle);
    }

    private void solvePolar() {
        angle = atan(deltaY / deltaX);
        if (deltaX < 0) {
            angle += PI;
        }
        if (angle < 0) {
            angle += 2 * PI;
        }
        if (angle == (2 * PI)) {
            angle = 0;
        }
        magnitude = sqrt(pow(deltaX, 2) + pow(deltaY, 2));
    }

    private void solveStandard() {
        deltaX = magnitude * cos(angle);
        deltaY = magnitude * sin(angle);
    }

    public double deltaX() {
        return deltaX;
    }

    public void setDeltaX(double deltaX) {
        this.deltaX = deltaX;
        solvePolar();
    }

    public double deltaY() {
        return deltaY;
    }

    public void setDeltaY(double deltaY) {
        this.deltaY = deltaY;
        solvePolar();
    }

    public double magnitude() {
        return magnitude;
    }

    public void setMagnitude(double magnitude) {
        this.magnitude = magnitude;
        solveStandard();
    }

    public double angle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
        solveStandard();
    }

    public Vector copy() {
        return createStandard(deltaX, deltaY);
    }

    public Point getEndpoint(Point start) {
        return new Point(start.x + deltaX, start.y + deltaY);
    }

    public Vector scale(double level) {
        return Vector.createStandard(deltaX * level, deltaY * level);
    }

    @Override
    public String toString() {
        return "Vector [deltaX=" + deltaX + ", deltaY=" + deltaY + ", magnitude=" + magnitude + ", angle=" + angle + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(angle);
        result = (prime * result) + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(magnitude);
        result = (prime * result) + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Vector other = (Vector) obj;
        if (Double.doubleToLongBits(angle) != Double.doubleToLongBits(other.angle)) {
            return false;
        }
        if (Double.doubleToLongBits(magnitude) != Double.doubleToLongBits(other.magnitude)) {
            return false;
        }
        return true;
    }
}
package elektoh.game.utils.geometry;

import static java.lang.Math.round;
import elektoh.game.utils.MathUtils;

public class Point {
    public static final Point ORIGIN = new Point(0, 0);
    public final double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point translate(double xMod, double yMod) {
        return translate(new Point(xMod, yMod));
    }

    public Point translate(Point p) {
        return new Point(x + p.x, y + p.y);
    }

    public Point scale(double trans) {
        return scale(trans, trans);
    }

    public Point scale(double transX, double transY) {
        return new Point(x * transX, y * transY);
    }

    public Point rotate(Point center, double angle) {
        Vector v = Vector.createStandard(center, this);
        v.setAngle(v.angle() + angle);
        return v.getEndpoint(center);
    }

    public boolean collides(Point point) {
        return equals(point);
    }

    public boolean collides(Edge edge) {
        Vector s2f = Vector.createStandard(edge.start, edge.finish);
        Vector s2p = Vector.createStandard(edge.start, this);
        if (equals(edge.start) || equals(edge.finish)) {
            return true;
        }
        if (MathUtils.roundTo(8, s2f.angle()) == MathUtils.roundTo(8, s2p.angle())) {
            if (s2f.magnitude() > s2p.magnitude()) {
                return true;
            }
        }
        return false;
    }

    public boolean collides(Polygon polygon) {
        int count = 0;
        Edge ray = new Edge(this, translate(Integer.MAX_VALUE, 0));
        for (Edge edge : polygon.getEdges()) {
            if (ray.collides(edge)) {
                count++;
            }
        }
        return !(count % 2 == 0);
    }

    public Point roundTo(int places) {
        return new Point(MathUtils.roundTo(places, x), MathUtils.roundTo(places, y));
    }

    public Coord toCoord() {
        return new Coord((int) round(x), (int) round(y));
    }

    public Point copy() {
        return new Point(x, y);
    }

    @Override
    public String toString() {
        return "Point [x=" + x + ", y=" + y + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (prime * result) + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = (prime * result) + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Point other = (Point) obj;
        if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        return true;
    }
}

package elektoh.game.utils.geometry;

public class Coord {
    public final int x, y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coord move(int xMod, int yMod) {
        int newX = x + xMod;
        int newY = y + yMod;
        return new Coord(newX, newY);
    }

    public static Coord[] moveArray(int xMod, int yMod, Coord[] cArray) {
        Coord[] newCoords = new Coord[cArray.length];
        for (int i2 = 0; i2 < cArray.length; i2++) {
            newCoords[i2] = cArray[i2].move(xMod, yMod);
        }
        return newCoords;
    }

    @Override
    public String toString() {
        return x + "," + y;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + x;
        result = (prime * result) + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Coord other = (Coord) obj;
        if (x != other.x) {
            return false;
        }
        if (y != other.y) {
            return false;
        }
        return true;
    }
}

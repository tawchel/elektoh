package elektoh.game.utils.geometry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Polygon {
    private final List<Edge> edges;
    private final List<Point> points;
    private boolean complete;
    private double leftBound, rightBound, bottomBound, topBound;

    public Polygon(Edge... edges) {
        this(Arrays.asList(edges));
    }

    public Polygon(List<Edge> edges) {
        this.edges = edges;
        complete = edges.get(0).start.equals(edges.get(edges.size() - 1).finish);
        for (int i = 1; i < (edges.size() - 1); i++) {
            Edge current = edges.get(i);
            Edge next = edges.get(i + 1);
            if (!current.finish.equals(next.start)) {
                complete = false;
            }
        }
        points = new ArrayList<Point>();
        for (Edge edge : edges) {
            if (!points.contains(edge.start)) {
                points.add(edge.start);
            }
            if (!points.contains(edge.finish)) {
                points.add(edge.finish);
            }
        }
        double firstX = points.get(0).x;
        double firstY = points.get(0).y;
        leftBound = firstX;
        rightBound = firstX;
        bottomBound = firstY;
        topBound = firstY;
        for (Point p : points) {
            double x = p.x;
            double y = p.y;
            if (x < leftBound) {
                leftBound = x;
            }
            if (x > rightBound) {
                rightBound = x;
            }
            if (y < bottomBound) {
                bottomBound = y;
            }
            if (y > topBound) {
                topBound = y;
            }
        }
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public List<Point> getPoints() {
        return points;
    }

    public boolean complete() {
        return complete;
    }

    public Polygon translate(double xMod, double yMod) {
        return translate(new Point(xMod, yMod));
    }

    public Polygon translate(Point p) {
        List<Edge> newEdges = new ArrayList<Edge>();
        for (Edge edge : edges) {
            newEdges.add(edge.translate(p));
        }
        return new Polygon(newEdges);
    }

    public Polygon scale(double trans) {
        return scale(trans, trans);
    }

    public Polygon scale(double transX, double transY) {
        List<Edge> newEdges = new ArrayList<Edge>();
        for (Edge edge : edges) {
            newEdges.add(edge.scale(transX, transY));
        }
        return new Polygon(newEdges);
    }

    public Polygon rotate(double angle) {
        return rotate(getCenter(), angle);
    }

    public Polygon rotate(Point center, double angle) {
        List<Edge> newEdges = new ArrayList<Edge>();
        for (Edge edge : edges) {
            newEdges.add(edge.rotate(center, angle));
        }
        return new Polygon(newEdges);
    }

    public boolean collides(Point point) {
        return point.collides(this);
    }

    public boolean collides(Edge edge) {
        return edge.collides(this);
    }

    public boolean collides(Polygon polygon) {
        boolean yes = false;
        for (Point point : getPoints()) {
            if (point.collides(polygon)) {
                yes = true;
            }
        }
        return yes;
    }

    public Point getCenter() {
        double xTotal = 0;
        double yTotal = 0;
        for (Point p : points) {
            xTotal += p.x;
            yTotal += p.y;
        }
        return new Point(xTotal / points.size(), yTotal / points.size());
    }

    public Polygon roundTo(int places) {
        List<Edge> newEdges = new ArrayList<Edge>();
        for (Edge edge : edges) {
            newEdges.add(edge.roundTo(places));
        }
        return new Polygon(newEdges);
    }

    public static Polygon getSquare(Point origin, double size) {
        return getRectangle(origin, size, size);
    }

    public static Polygon getRectangle(Point origin, double sizeX, double sizeY) {
        sizeX /= 2;
        sizeY /= 2;
        Point p1 = new Point(origin.x - sizeX, origin.y - sizeY);
        Point p2 = new Point(origin.x - sizeX, origin.y + sizeY);
        Point p3 = new Point(origin.x + sizeX, origin.y + sizeY);
        Point p4 = new Point(origin.x + sizeX, origin.y - sizeY);
        Edge e1 = new Edge(p1, p2);
        Edge e2 = new Edge(p2, p3);
        Edge e3 = new Edge(p3, p4);
        Edge e4 = new Edge(p4, p1);
        return new Polygon(e1, e2, e3, e4);
    }

    public double getLeftBound() {
        return leftBound;
    }

    public double getRightBound() {
        return rightBound;
    }

    public double getBottomBound() {
        return bottomBound;
    }

    public double getTopBound() {
        return topBound;
    }

    public double getWidth() {
        return getRightBound() - getLeftBound();
    }

    public double getHeight() {
        return getBottomBound() - getTopBound();
    }

    @Override
    public String toString() {
        return "Polygon [complete=" + complete + ", x:" + leftBound + "-" + rightBound + ", y:" + bottomBound + "-" + topBound + "]";
    }
}
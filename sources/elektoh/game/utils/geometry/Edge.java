package elektoh.game.utils.geometry;

import static java.lang.Math.abs;
import elektoh.game.utils.Meme;

public class Edge {
    public final Point start, finish;

    public Edge(double x1, double y1, double x2, double y2) {
        this(new Point(x1, y1), new Point(x2, y2));
    }

    public Edge(Point start, Point finish) {
        this.start = start;
        this.finish = finish;
    }

    public Edge translate(double xMod, double yMod) {
        return translate(new Point(xMod, yMod));
    }

    public Edge translate(Point p) {
        return new Edge(start.translate(p), finish.translate(p));
    }

    public Edge scale(double trans) {
        return scale(trans, trans);
    }

    public Edge scale(double transX, double transY) {
        return new Edge(start.scale(transX, transY), finish.scale(transX, transY));
    }

    public Edge rotate(double angle) {
        return rotate(getCenter(), angle);
    }

    public Edge rotate(Point center, double angle) {
        return new Edge(start.rotate(center, angle), finish.rotate(center, angle));
    }

    public boolean collides(Point point) {
        return point.collides(this);
    }

    @Meme("http://memeblender.com/wp-content/uploads/2012/05/meme-comics-understanding-math.jpg")
    public boolean collides(Edge edge) {
        double m1 = slope();
        double b1 = yIntercept();
        double m2 = edge.slope();
        double b2 = edge.yIntercept();
        if (m1 == m2 && b1 == b2) {
            return true;
        }
        if (abs(m1) == abs(m2)) {
            return false;
        }
        double sharedX = (b2 - b1) / (m1 - m2);
        double sharedY = (m1 * sharedX) + b1;
        Point collision = new Point(sharedX, sharedY);
        if (!collides(collision) || !edge.collides(collision)) {
            return false;
        }
        return true;
    }

    public boolean collides(Polygon polygon) {
        boolean yes = false;
        for (Edge edge : polygon.getEdges()) {
            if (collides(edge)) {
                yes = true;
            }
        }
        return yes;
    }

    public Point getCenter() {
        return new Point((start.x + finish.x) / 2, (start.y + finish.y) / 2);
    }

    public Edge roundTo(int places) {
        return new Edge(start.roundTo(places), finish.roundTo(places));
    }

    private double slope() {
        double xDiff = finish.x - start.x;
        double yDiff = finish.y - start.y;
        if (xDiff == 0) {
            return Integer.MAX_VALUE;
        }
        return yDiff / xDiff;
    }

    private double yIntercept() {
        return start.y - (slope() * start.x);
    }

    @Override
    public String toString() {
        return "Edge [start=" + start + ", finish=" + finish + "]";
    }

    /**
     * Important! For equals() & hashCode() The order of start & finish does not matter.
     * Return true if:
     * start==o.start finish==o.finish
     * ||
     * start==o.finish finish==o.start
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        Point A;
        Point B;
        if (start.hashCode() >= finish.hashCode()) {
            A = start;
            B = finish;
        } else {
            A = finish;
            B = start;
        }
        int result = 1;
        result = (prime * result) + ((finish == null) ? 0 : A.hashCode());
        result = (prime * result) + ((start == null) ? 0 : B.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Edge other = (Edge) obj;
        if (finish == null) {
            if (other.finish != null) {
                return false;
            }
        } else if (!finish.equals(other.finish) && !finish.equals(other.start)) {
            return false;
        }
        if (start == null) {
            if (other.start != null) {
                return false;
            }
        } else if (!start.equals(other.start) && !start.equals(other.finish)) {
            return false;
        }
        return true;
    }
}
package elektoh.game.utils.geometry;

import static java.lang.Math.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import elektoh.game.utils.Function;

public class HexagonMap {
    public final int size;
    public final int dimX;
    public final int dimY;
    protected final double radius;
    protected final double width;
    protected final double height;
    protected final Function<Coord, Polygon> hexagons;
    protected List<Point> allPoints = new ArrayList<Point>();
    protected List<Edge> allEdges = new ArrayList<Edge>();
    protected List<Polygon> allHexagons = new ArrayList<Polygon>();

    public HexagonMap(int size) {
        this.size = size;
        dimY = size;
        radius = 0.7d / dimY;
        height = radius * 2;
        width = (sqrt(3) / 2) * height;
        dimX = (int) Math.ceil(1d / width);
        hexagons = new Function<Coord, Polygon>();
    }

    public void create() {
        for (int x = 0; x < dimX; x++) {
            for (int y = 0; y < dimY; y++) {
                Coord c = new Coord(x, y);
                hexagons.put(c, createHexagon(c));
            }
        }
        for (Polygon hexagon : hexagons.collectionB()) {
            allHexagons.add(hexagon);
            for (Point p : hexagon.getPoints()) {
                if (!allPoints.contains(p)) {
                    allPoints.add(p);
                }
            }
            for (Edge edge : hexagon.getEdges()) {
                if (!allEdges.contains(edge)) {
                    allEdges.add(edge);
                }
            }
        }
    }

    private Polygon createHexagon(Coord c) {
        List<Point> points = new ArrayList<Point>();
        List<Edge> edges = new ArrayList<Edge>();
        double offset = (c.y % 2) == 0 ? width / 2 : 0;
        Point center = new Point((c.x * width) + offset, (c.y * 0.75d * height));
        for (double theta = PI / 6; theta < (2 * PI); theta += PI / 3) {
            Vector v = Vector.createPolar(radius, theta + (PI / 3));
            points.add(v.getEndpoint(center).roundTo(12));
        }
        for (int i = 0; i < points.size(); i++) {
            Point current = points.get(i);
            Point next;
            if (i == (points.size() - 1)) {
                next = points.get(0);
            } else {
                next = points.get(i + 1);
            }
            Edge edge = new Edge(current, next);
            edges.add(edge);
        }
        return new Polygon(edges);
    }

    public boolean outOfBounds(Coord c) {
        if ((c.x < 0) || (c.y < 0)) {
            return true;
        }
        if ((c.x > size) || (c.y > size)) {
            return true;
        }
        return false;
    }

    public List<Point> getAllPoints() {
        return allPoints;
    }

    public List<Edge> getAllEdges() {
        return allEdges;
    }

    public List<Polygon> getAllHexagons() {
        return allHexagons;
    }

    public Collection<Coord> getAllCoords() {
        return hexagons.collectionA();
    }

    public List<Coord> getAdjacentHexagons(Coord c) {
        List<Coord> adjacentCoords = new ArrayList<Coord>();
        adjacentCoords.add(c.move(+1, 0));
        adjacentCoords.add(c.move(-1, 0));
        adjacentCoords.add(c.move(0, +1));
        adjacentCoords.add(c.move(0, -1));
        if ((c.y % 2) == 0) {
            adjacentCoords.add(c.move(+1, -1));
            adjacentCoords.add(c.move(-1, -1));
        } else {
            adjacentCoords.add(c.move(+1, +1));
            adjacentCoords.add(c.move(-1, +1));
        }
        return adjacentCoords;
    }

    public Coord getHexCoord(Polygon hexagon) {
        return hexagons.getFromB(hexagon);
    }

    public Polygon getHexagon(Coord c) {
        return hexagons.getFromA(c);
    }
}
package elektoh.game.utils;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {
    private static Logger log;

    public static void create() {
        log = Logger.getLogger("Elektoh");
        for (Handler handler : log.getParent().getHandlers()) {
            log.getParent().removeHandler(handler);
        }
        log.addHandler(new ConsoleHandler());
        // In case we ever want a saved log
        // log.addHandler(new FileHandler());
        for (Handler handler : log.getHandlers()) {
            handler.setFormatter(new LogFormatter());
        }
    }

    public static void log(Level lvl, Object... message) {
        if (message.length == 1) {
            if (message[0] == null) {
                log.log(lvl, "nuuuull");
                return;
            }
            log.log(lvl, message[0].toString());
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append('\n');
        for (int i = 0; i < message.length; i++) {
            sb.append(message[i].toString());
            if ((i + 1) != message.length) {
                sb.append('\n');
            }
        }
        log.log(lvl, sb.toString());
    }

    public static void info(Object... message) {
        log(Level.INFO, message);
    }

    public static void severe(Object... message) {
        log(Level.SEVERE, message);
    }

    public static void warning(Object... message) {
        log(Level.WARNING, message);
    }

    public static void config(Object... message) {
        log(Level.CONFIG, message);
    }

    public static void fine(Object... message) {
        log(Level.FINE, message);
    }

    public static void finer(Object... message) {
        log(Level.FINER, message);
    }

    public static void finest(Object... message) {
        log(Level.FINEST, message);
    }

    public static void test() {
        info("test");
    }
}
package elektoh.game.utils;

import java.io.File;

import elektoh.game.Elektoh;

public class Directories {
    public static final String DIRECTORY = Elektoh.getGameDir().getAbsolutePath() + File.separator;
    public static final String SAVES = DIRECTORY + "saves" + File.separator;

    public static String resources(String id) {
        return "assets/" + id;
    }

    public static String textures(String id) {
        return resources(id) + "/textures";
    }

    public static String fonts(String id) {
        return resources(id) + "/fonts";
    }

    public static String language(String id) {
        return resources(id) + "/lang";
    }

    public static void make() {
        new File(Directories.DIRECTORY + "saves").mkdir();
    }
}
package elektoh.game.utils;

import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;

public class MathUtils {
    public static double center(int size, int largerSize) {
        return (largerSize - size) / 2D;
    }

    public static int[] getCoord(int num, int columns) {
        columns -= 1;
        if (num > columns) {
            double rows = (double) num / (double) columns;
            double trueColumns = (rows - Math.floor(rows)) * columns;
            return new int[] { (int) (Math.round(trueColumns) - 1), (int) Math.floor(rows) };
        } else {
            return new int[] { num, 0 };
        }
    }

    public static double roundTo(int places, Number orig) {
        double rounded = orig.doubleValue();
        double roundTo = Math.pow(10, places);
        rounded *= roundTo;
        rounded = Math.round(rounded);
        rounded /= roundTo;
        return rounded;
    }

    public static double cosineInterpolation(double a, double b, double x) {
        double ft = x * Math.PI;
        double f = (1 - Math.cos(ft)) * 0.5d;
        return (a * (1 - f)) + (b * f);
    }

    public static ByteBuffer createByteBuffer(int size) {
        return BufferUtils.createByteBuffer(size);
    }

    public static double contrast(double orig, double highestValue, double lowestValue) {
        double slope = 1 / (highestValue - lowestValue);
        double yIntercept = -(lowestValue * slope);
        return (slope * orig) + yIntercept;
    }

    public static double minMax(double orig, double min, double max) {
        if (orig < min) {
            return min;
        }
        if (orig > max) {
            return max;
        }
        return orig;
    }

    public static int round(double orig) {
        return (int) Math.round(orig);
    }
}
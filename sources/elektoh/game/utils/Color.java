package elektoh.game.utils;

import static org.lwjgl.opengl.GL11.glColor4d;

public class Color {
    public static final Color WHITE = new Color(1, 1, 1);
    public static final Color GREY = new Color(0.5D, 0.5D, 0.5D);
    public static final Color BLACK = new Color(0, 0, 0);
    public static final Color RED = new Color(1, 0, 0);
    public static final Color GREEN = new Color(0, 1, 0);
    public static final Color BLUE = new Color(0, 0, 1);

    public static class Swatches {
        public static final Color GLOW = new Color(0x2cc2f9);
    }

    public final double r;
    public final double g;
    public final double b;
    public final double alpha;
    public final int hex;

    public Color(int hex) {
        this(hex, 1);
    }

    public Color(int hex, double alpha) {
        this.hex = hex;
        this.alpha = alpha;
        double[] rgb = hexToRGB(hex);
        r = rgb[0];
        g = rgb[1];
        b = rgb[2];
    }

    public Color(double r, double g, double b) {
        this(r, g, b, 1);
    }

    public Color(double r, double g, double b, double alpha) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.alpha = alpha;
        hex = (((int) (alpha * 255) & 0xFF) << 24) | ((int) (r * 255) << 16) | (((int) (g * 255) & 0xFF) << 8) | (((int) (b * 255) & 0xFF) << 0);
    }

    public void bind() {
        bind(alpha);
    }

    public void bind(double alpha) {
        glColor4d(r, g, b, alpha);
    }

    public Color opacity(double alpha) {
        return new Color(r, g, b, alpha);
    }

    public static double[] hexToRGB(int hex) {
        double[] rgb = new double[3];
        rgb[0] = (double) ((hex >> 16) & 255) / 255.0F;
        rgb[1] = (double) ((hex >> 8) & 255) / 255.0F;
        rgb[2] = (double) (hex & 255) / 255.0F;
        return rgb;
    }

    public java.awt.Color toAwt() {
        return new java.awt.Color((float) r, (float) g, (float) b, (float) alpha);
    }

    public static Color greyScale(double shade) {
        return new Color(shade, shade, shade);
    }

    public Color mix(Color color) {
        return new Color((r + color.r) / 2d, (g + color.g) / 2d, (b + color.b) / 2d, (alpha + color.alpha) / 2d);
    }
}
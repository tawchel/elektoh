package elektoh.game.utils;

public @interface Meme {
    String value();

    String reason() default "Dear decompile nerds we are very serious people";
}
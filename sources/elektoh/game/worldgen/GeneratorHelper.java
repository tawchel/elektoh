package elektoh.game.worldgen;

import static java.lang.Math.*;

import java.util.LinkedList;
import java.util.Queue;

import elektoh.game.map.World;
import elektoh.game.tile.Tile;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Edge;
import elektoh.game.utils.geometry.Point;

public class GeneratorHelper {
    public static void rasterizeEdge(World world, TerrainType newTerrain, Tile tile, Edge edge) {
        Point start = edge.start;
        Point finish = edge.finish;
        double deltaX = finish.x - start.x;
        double deltaY = finish.y - start.y;
        double m = deltaY / deltaX;
        boolean negX = deltaX < 0;
        boolean negY = deltaY < 0;
        world.setTile(tile, start.toCoord());
        world.setTile(tile, finish.toCoord());
        if (Math.abs(m) <= 1) {
            double x = start.x;
            double y = start.y;
            while (x != finish.x) {
                if (!negX) {
                    x++;
                }
                if (negX) {
                    x--;
                }
                if (!negX) {
                    y += m;
                }
                if (negX) {
                    y -= m;
                }
                Coord c = new Coord((int) round(x), (int) round(y));
                world.setTile(tile, c);
                world.setTerrainType(newTerrain, c);
            }
        }
        if (abs(m) > 1) {
            double x = start.x;
            double y = start.y;
            while (y != finish.y) {
                if (!negY) {
                    y++;
                }
                if (negY) {
                    y--;
                }
                if (!negY) {
                    x += Math.pow(m, -1);
                }
                if (negY) {
                    x -= Math.pow(m, -1);
                }
                Coord c = new Coord((int) round(x), (int) round(y));
                world.setTile(tile, c);
                world.setTerrainType(newTerrain, c);
            }
        }
    }

    public static void floodFill(World world, Coord start, TerrainType newTerrain, Tile newTile, Tile replace) {
        Queue<Coord> queue = new LinkedList<Coord>();
        queue.add(start);
        while (!queue.isEmpty() && (queue.size() < 2000)) {
            Coord current = queue.remove();
            if (world.getTile(current) == replace) {
                world.setTile(newTile, current);
                world.setTerrainType(newTerrain, current);
                queue.add(current.move(+1, 0));
                queue.add(current.move(-1, 0));
                queue.add(current.move(0, +1));
                queue.add(current.move(0, -1));
            }
        }
    }
}

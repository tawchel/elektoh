package elektoh.game.worldgen;

import static java.lang.Math.PI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Edge;
import elektoh.game.utils.geometry.HexagonMap;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;
import elektoh.game.utils.geometry.Vector;
import elektoh.game.utils.noise.PerlinNoise1D;

public class AlteredHexagonMap extends HexagonMap {
    private final Random rand;
    private final Map<Point, Vector> pointAlterations;
    private final Map<Edge, List<Edge>> edgeAlterations;
    private final Map<Coord, Polygon> alteredHexagons;
    private final double magnitudeModifier;
    private final double edgeContrast;

    public AlteredHexagonMap(Random rand, int size, double magnitudeModifier, double edgeContrast) {
        super(size);
        this.rand = rand;
        pointAlterations = new HashMap<Point, Vector>();
        edgeAlterations = new HashMap<Edge, List<Edge>>();
        alteredHexagons = new HashMap<Coord, Polygon>();
        this.magnitudeModifier = magnitudeModifier;
        this.edgeContrast = edgeContrast;
    }

    @Override
    public void create() {
        super.create();
        for (Point p : allPoints) {
            double magnitude = rand.nextDouble() * (radius * magnitudeModifier);
            double angle = rand.nextDouble() * (2 * Math.PI);
            pointAlterations.put(p, Vector.createPolar(magnitude, angle));
        }
        for (Edge edge : allEdges) {
            int size = rand.nextInt(4) + 4;
            PerlinNoise1D noise = new PerlinNoise1D(size, rand.nextLong(), new int[] { size, size / 2, size / 4 });
            noise.generateNoise();
            List<Edge> alteredEdge = new ArrayList<Edge>();
            Point start = getAlteredPoint(edge.start);
            Point finish = getAlteredPoint(edge.finish);
            Vector v1 = Vector.createStandard(start, finish);
            Point p1 = start;
            Point p2 = start;
            for (int i = 1; i < noise.getSize(); i++) {
                Vector v2 = v1.copy();
                v2.setMagnitude(v1.magnitude() * (1d / (noise.getSize() + 1)));
                Vector v3 = Vector.createPolar((noise.getValue(i) - 0.5d) / edgeContrast, v2.angle() + (PI / 2));
                Point p3 = v2.getEndpoint(p1);
                Point p4 = v3.getEndpoint(p3);
                alteredEdge.add(new Edge(p2, p4));
                p1 = p3;
                p2 = p4;
            }
            alteredEdge.add(new Edge(p2, finish));
            edgeAlterations.put(edge, alteredEdge);
        }
        for (Polygon polygon : allHexagons) {
            Coord c = getHexCoord(polygon);
            List<Edge> newEdges = new ArrayList<Edge>();
            for (Edge edge : polygon.getEdges()) {
                newEdges.addAll(getAlteredEdge(edge));
            }
            alteredHexagons.put(c, new Polygon(newEdges));
        }
    }

    public Point getAlteredPoint(Point p) {
        Vector v = pointAlterations.get(p);
        return v.getEndpoint(p).roundTo(12);
    }

    public List<Edge> getAlteredEdge(Edge edge) {
        return edgeAlterations.get(edge);
    }

    public Map<Coord, Polygon> getAlteredHexagons() {
        return alteredHexagons;
    }
}
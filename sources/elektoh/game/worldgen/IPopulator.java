package elektoh.game.worldgen;

import elektoh.game.map.World;
import elektoh.game.worldgen.WorldGenerator;

public interface IPopulator {
    public void populate(WorldGenerator generator, World world);
}
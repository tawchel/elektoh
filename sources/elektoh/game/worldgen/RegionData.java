package elektoh.game.worldgen;

import elektoh.game.utils.geometry.Coord;

public class RegionData {
    private final Coord coord;
    private TerrainType terrain;
    private String structureName;

    public RegionData(Coord coord) {
        this.coord = coord;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setTerrain(TerrainType terrain) {
        this.terrain = terrain;
    }

    public TerrainType getTerrainType() {
        return terrain;
    }

    public void setStructureName(String structureName) {
        this.structureName = structureName;
    }

    public String getStructureName() {
        return structureName;
    }

    @Override
    public String toString() {
        return "RegionData [coord=" + coord + ", terrain=" + terrain + ", structureName=" + structureName + "]";
    }
}
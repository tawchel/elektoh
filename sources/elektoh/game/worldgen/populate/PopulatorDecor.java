package elektoh.game.worldgen.populate;

import elektoh.game.entities.EntityCactus;
import elektoh.game.map.World;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;
import elektoh.game.worldgen.IPopulator;
import elektoh.game.worldgen.WorldGenerator;

public class PopulatorDecor implements IPopulator {
    @Override
    public void populate(WorldGenerator generator, World world) {
        for (Coord c : generator.hexMap.getAllCoords()) {
            Polygon hexagon = generator.hexMap.getHexagon(c);
            Point p = hexagon.getCenter().scale(world.data.tileSize);
            EntityCactus cactus = new EntityCactus(world);
            cactus.create(p);
        }
    }
}
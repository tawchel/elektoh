package elektoh.game.worldgen;

import elektoh.game.tile.Tile;
import elektoh.game.tile.Tiles;
import elektoh.game.utils.Color;

public enum TerrainType {
    OCEAN(false, null),
    LAKE(false, null),
    BEACH(true, null),
    PLAINS(true, new Color(0x7ff572)),
    RAINFOREST(true, new Color(0x3ec14a)),
    SAVANNA(true, new Color(0xd5b96e)),
    DESERT(true, new Color(0xc6c0a2)),
    FOREST(true, new Color(0x69e65b)),
    TUNDRA(true, new Color(0xffffff)),
    TAIGA(true, new Color(0xa6e8e0)),
    QUARRY(true, null);
    public static final TerrainType[][] biomeMap = new TerrainType[][] {
//@formatter:off
/** Precipitation(up/down) & Heat(left/right)
    This is in spreadsheet format for readability and edit-ability. DO NOT UNFORMAT.
                                           5            4          3          2           1    **/
/**      4      **/ new TerrainType[]{ RAINFOREST, RAINFOREST,    FOREST,      TAIGA,      TAIGA},
/**      3      **/ new TerrainType[]{ RAINFOREST,     PLAINS,    FOREST,     FOREST,      TAIGA},
/**      2      **/ new TerrainType[]{    SAVANNA,    SAVANNA,    PLAINS,     PLAINS,     TUNDRA},
/**      1      **/ new TerrainType[]{     DESERT,     DESERT,   SAVANNA,     TUNDRA,     TUNDRA},
//@formatter:on
    };
    private final boolean isLand;
    private final Color grassColor;

    TerrainType(boolean isLand, Color color) {
        this.isLand = isLand;
        grassColor = color;
    }

    public boolean isLand() {
        return isLand;
    }

    public Color grassColor() {
        return grassColor;
    }

    public Tile getTile() {
        switch (this) {
            case OCEAN:
                return Tiles.ocean;
            case LAKE:
                return Tiles.ocean;
            case DESERT:
                return Tiles.sand;
            case BEACH:
                return Tiles.sand;
            case QUARRY:
                return Tiles.stone;
            default:
                return Tiles.grass;
        }
    }
}
package elektoh.game.worldgen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import elektoh.game.map.World;
import elektoh.game.tile.Tile;
import elektoh.game.tile.Tiles;
import elektoh.game.utils.Log;
import elektoh.game.utils.MathUtils;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Edge;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;
import elektoh.game.utils.noise.BiasType;
import elektoh.game.utils.noise.BiasedPerlinNoise2D;
import elektoh.game.utils.noise.Noise2D;
import elektoh.game.utils.noise.PerlinNoise2D;

public class WorldGenerator {
    private static List<IPopulator> populators = new ArrayList<IPopulator>();
    public final Random rand;
    public AlteredHexagonMap hexMap;
    public Map<Coord, RegionData> regionData;
    public Noise2D islandNoise;
    public Noise2D temperatureNoise;
    public Noise2D precipitationNoise;

    public WorldGenerator(Random rand) {
        this.rand = rand;
        regionData = new HashMap<Coord, RegionData>();
    }

    public void generateAssets() {
        hexMap = new AlteredHexagonMap(rand, 32, 0.4d, 180d);
        hexMap.create();
        int noiseSize = 200;
        int[] octaves = new int[] { Math.round(noiseSize / 16f), Math.round(noiseSize / 32f), Math.round(noiseSize / 64f), Math.round(noiseSize / 128f), Math.round(noiseSize / 256f) };
        islandNoise = new BiasedPerlinNoise2D(noiseSize, rand.nextLong(), octaves, BiasType.CONCENTRATED, 2.0f);
        islandNoise.generateNoise();
        noiseSize = 80;
        octaves = new int[] { Math.round(noiseSize / 16f), Math.round(noiseSize / 32f), Math.round(noiseSize / 64f) };
        temperatureNoise = new BiasedPerlinNoise2D(noiseSize, rand.nextLong(), octaves, BiasType.GRADIENT, 4);
        temperatureNoise.generateNoise();
        precipitationNoise = new PerlinNoise2D(noiseSize, rand.nextLong(), octaves);
        precipitationNoise.generateNoise();
        generateRegionData();
    }

    private void generateRegionData() {
        for (Coord c : hexMap.getAllCoords()) {
            Polygon hexagon = hexMap.getHexagon(c);
            // pun not intended
            boolean isLand = false;
            isLand = getAverageValue(islandNoise, hexagon) > (islandNoise.getAverage() * 1.1f);
            if (hexMap.outOfBounds(c)) {
                isLand = false;
            }
            TerrainType terrain = TerrainType.OCEAN;
            if (isLand) {
                terrain = determineTerrain(getAverageValue(temperatureNoise, hexagon), getAverageValue(precipitationNoise, hexagon));
                for (Coord adjC : hexMap.getAdjacentHexagons(c)) {
                    if (!(getAverageValue(islandNoise, hexMap.getHexagon(adjC)) > (islandNoise.getAverage() * 1.1f))) {
                        if (rand.nextDouble() > 0.6d) {
                            terrain = TerrainType.BEACH;
                        }
                    }
                }
            }
            RegionData region = new RegionData(c);
            region.setTerrain(terrain);
            regionData.put(c, region);
        }
        int quarries = rand.nextInt(3) + 4;
        while (quarries > 0) {
            Coord c = new Coord(rand.nextInt(hexMap.dimX), rand.nextInt(hexMap.dimY));
            RegionData region = regionData.get(c);
            if (region.getTerrainType().isLand()) {
                region.setTerrain(TerrainType.QUARRY);
                for (Coord adjC : hexMap.getAdjacentHexagons(c)) {
                    RegionData adjRegion = regionData.get(adjC);
                    if ((adjRegion != null) && adjRegion.getTerrainType().isLand()) {
                        if (rand.nextDouble() >= 0.7d) {
                            adjRegion.setTerrain(TerrainType.QUARRY);
                        }
                    }
                }
                quarries--;
            }
        }
    }

    private TerrainType determineTerrain(float tempValue, float precipValue) {
        tempValue = (float) MathUtils.contrast(tempValue, 0.25d, 0.75d);
        tempValue = (float) MathUtils.minMax(tempValue, 0, 1);
        tempValue *= 4;
        precipValue = (float) MathUtils.contrast(precipValue, 0.3d, 0.7d);
        precipValue = (float) MathUtils.minMax(precipValue, 0, 1);
        precipValue *= 3;
        int temp = Math.round(tempValue);
        int precip = Math.round(precipValue);
        return TerrainType.biomeMap[precip][temp];
    }

    private float getAverageValue(Noise2D islandNoise, Polygon polygon) {
        float average = 0;
        int count = 0;
        if (polygon == null) {
            return 0;
        }
        for (Point p : polygon.getPoints()) {
            average += islandNoise.getValueAtPoint(p.x, p.y);
            count++;
        }
        return average / count;
    }

    public void rasterize(World world) {
        int index = 0;
        int total = hexMap.getAlteredHexagons().size();
        for (Entry<Coord, Polygon> entry : hexMap.getAlteredHexagons().entrySet()) {
            Polygon hexagon = entry.getValue();
            Coord c = entry.getKey();
            RegionData region = regionData.get(c);
            if (region.getTerrainType().isLand()) {
                Polygon resized = hexagon.scale(world.data.tileSize).roundTo(0);
                Tile tile = region.getTerrainType().getTile();
                for (Edge edge : resized.getEdges()) {
                    GeneratorHelper.rasterizeEdge(world, region.getTerrainType(), tile, edge);
                }
                GeneratorHelper.floodFill(world, resized.getCenter().toCoord(), region.getTerrainType(), tile, Tiles.ocean);
            }
            index++;
            Log.info(index + " / " + total + " or " + (MathUtils.roundTo(3, index / (double) total) * 100) + "% completed polygons");
        }
    }

    public void populate(World world) {
        for (IPopulator populator : populators) {
            populator.populate(this, world);
        }
    }

    public static void addPopulator(IPopulator populator) {
        populators.add(populator);
    }
}
package elektoh.game.map;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.google.gson.annotations.SerializedName;

import elektoh.game.io.Camara;
import elektoh.game.savedata.TileDictionary;
import elektoh.game.tile.Tile;
import elektoh.game.tile.Tiles;
import elektoh.game.utils.GsonHelper;
import elektoh.game.utils.Meme;
import elektoh.game.utils.geometry.Coord;

public class WorldData {
    @Meme("http://i.imgur.com/oY43jQ3.jpg")
    public String name;
    public int chunkDim;
    @SerializedName("Tile Dictionary")
    public TileDictionary dictionary;
    public transient Random rand;
    public transient int tileSize;
    public transient File saveDir;
    public transient File dataSave;
    public transient File chunkDir;
    public transient Camara camara;
    public transient Tile defaultTile = Tiles.ocean;
    public transient Map<Coord, Chunk> chunks = new HashMap<Coord, Chunk>();

    public static File getDataSave(File saveDirectory) {
        return new File(saveDirectory.getAbsolutePath() + File.separator + "data.cfg");
    }

    public static File getChunkDir(File saveDirectory) {
        return new File(saveDirectory.getAbsolutePath() + File.separator + "chunks");
    }

    public static WorldData load(File saveDir) {
        WorldData tmp = null;
        try {
            tmp = GsonHelper.load(getDataSave(saveDir), WorldData.class);
            tmp.setup(saveDir);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tmp;
    }

    public static WorldData create(String worldName) {
        WorldData tmp = new WorldData();
        tmp.name = worldName;
        tmp.chunkDim = 10;
        tmp.dictionary = new TileDictionary();
        tmp.setup(World.getWorldDirFromName(worldName));
        return tmp;
    }

    private void setup(File saveDirectory) {
        rand = new Random(name.hashCode());
        saveDir = saveDirectory;
        dataSave = getDataSave(saveDir);
        chunkDir = getChunkDir(saveDir);
        tileSize = chunkDim * Chunk.CHUNK_SIZE;
    }
}
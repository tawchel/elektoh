package elektoh.game.map;

import static java.lang.Math.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import elektoh.game.entities.Entity;
import elektoh.game.entities.EntityPlayer;
import elektoh.game.io.Camara;
import elektoh.game.tile.Tile;
import elektoh.game.utils.Direction;
import elektoh.game.utils.Directories;
import elektoh.game.utils.GsonHelper;
import elektoh.game.utils.Meme;
import elektoh.game.utils.Log;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Vector;
import elektoh.game.worldgen.TerrainType;
import elektoh.game.worldgen.WorldGenerator;

public class World {
    public final WorldData data;

    public static World create(String name) {
        World world = new World(WorldData.create(name));
        WorldGenerator generator = new WorldGenerator(world.data.rand);
        generator.generateAssets();
        generator.rasterize(world);
        generator.populate(world);
        world.spawnPlayer();
        return world;
    }

    private void spawnPlayer() {
        boolean done = false;
        EntityPlayer player = new EntityPlayer(this, "player");
        while (!done) {
            Point pos = new Point(data.rand.nextDouble() * data.tileSize, data.rand.nextDouble() * data.tileSize);
            if (player.canMoveOnto(pos)) {
                done = true;
                player.create(pos);
            }
        }
    }

    private World(WorldData data) {
        this.data = data;
    }

    public void update() {
        List<Entity> entities = new ArrayList<Entity>();
        for (Entry<Coord, Chunk> entry : data.chunks.entrySet()) {
            Chunk chunk = entry.getValue();
            entities.addAll(chunk.getEntities());
            chunk.getEntities().clear();
        }
        for (Entity entity : entities) {
            entity.update();
            putEntityInChunk(entity);
        }
    }

    public void putEntityInChunk(Entity entity) {
        Chunk chunk = getChunk(getChunkCoord(entity.getCoord()));
        chunk.addEntity(entity);
    }

    public Tile getTileUnderPoint(Point p) {
        return getTile(new Coord((int) round(p.x), (int) round(p.y)));
    }

    public Tile getTile(int x, int y) {
        return getTile(new Coord(x, y));
    }

    public void setTile(Tile tile, int x, int y) {
        setTile(tile, new Coord(x, y));
    }

    public Tile getTile(Coord c) {
        if (!outOfBounds(c)) {
            Coord chunkCoord = getChunkCoord(c);
            Tile tile = getChunk(chunkCoord).getTile(getRelativeChunkCoord(c));
            if (tile == null) {
                return data.defaultTile;
            }
            return tile;
        }
        return null;
    }

    public void setTile(Tile tile, Coord c) {
        if (!outOfBounds(c)) {
            Coord chunkCoord = getChunkCoord(c);
            getChunk(chunkCoord).setTile(tile, getRelativeChunkCoord(c));
            if (!data.dictionary.containsTile(tile)) {
                data.dictionary.addTile(tile);
            }
        }
    }

    public TerrainType getTerrainType(Coord c) {
        if (!outOfBounds(c)) {
            Coord chunkCoord = getChunkCoord(c);
            return getChunk(chunkCoord).getTerrainType(getRelativeChunkCoord(c));
        }
        return null;
    }

    public void setTerrainType(TerrainType terrain, Coord c) {
        if (!outOfBounds(c)) {
            Coord chunkCoord = getChunkCoord(c);
            getChunk(chunkCoord).setTerrainType(terrain, getRelativeChunkCoord(c));
        }
    }

    public Coord getChunkCoord(Coord c) {
        int newX = (int) floor(c.x / (double) Chunk.CHUNK_SIZE);
        int newY = (int) floor(c.y / (double) Chunk.CHUNK_SIZE);
        return new Coord(newX, newY);
    }

    public Coord getRelativeChunkCoord(Coord c) {
        int newX = c.x % Chunk.CHUNK_SIZE;
        int newY = c.y % Chunk.CHUNK_SIZE;
        return new Coord(newX, newY);
    }

    public Chunk getChunk(Coord c) {
        if (data.chunks.get(c) == null) {
            data.chunks.put(c, new Chunk(this));
        }
        return data.chunks.get(c);
    }

    public List<Entity> getEntities(Point p, double radius) {
        List<Entity> entities = new ArrayList<Entity>();
        Coord posChunk = getChunkCoord(p.toCoord());
        for (int i = 0; i < Direction.values().length + 1; i++) {
            Coord chunkCoord;
            if (i < Direction.values().length) {
                Direction d = Direction.values()[i];
                chunkCoord = new Coord(posChunk.x + d.x, posChunk.y + d.y);
            } else {
                chunkCoord = posChunk;
            }
            for (Entity entity : getChunk(chunkCoord).getEntities()) {
                Vector v = Vector.createStandard(p, entity.getPosition());
                if (v.magnitude() <= radius) {
                    entities.add(entity);
                }
            }
        }
        return entities;
    }

    public boolean outOfBounds(Coord c) {
        return ((c.x < 0) || (c.y < 0) || (c.x > data.tileSize) || (c.y > data.tileSize));
    }

    public Camara getCamara() {
        return data.camara;
    }

    public void setCamara(Camara camara) {
        data.camara = camara;
    }

    public Set<Entry<Coord, Chunk>> getChunks() {
        return data.chunks.entrySet();
    }

    public void save() {
        data.saveDir.mkdir();
        data.chunkDir.mkdir();
        try {
            GsonHelper.save(data.dataSave, data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Entry<Coord, Chunk> e : data.chunks.entrySet()) {
            e.getValue().save(e.getKey());
        }
    }

    @Meme("http://i.imgur.com/NpLlsld.jpg")
    public static World load(File saveDir) {
        World world = null;
        try {
            world = new World(WorldData.load(saveDir));
            world.loadChunks();
        } catch (Exception e) {
            Log.warning("FAILED TO LOAD WORLD IN " + saveDir.getAbsolutePath() + ", Because:");
            e.printStackTrace();
        }
        return world;
    }

    private void loadChunks() {
        for (File chunk : data.chunkDir.listFiles()) {
            if (!chunk.getName().contains("entities")) {
                Chunk tmp = new Chunk(this);
                tmp.load(chunk);
                data.chunks.put(Chunk.getCoordFromFile(chunk), tmp);
            }
        }
    }

    public static File getWorldDirFromName(String worldName) {
        File tmp = new File(Directories.SAVES + worldName);
        tmp.mkdirs();
        return tmp;
    }

    public static Map<Date, String> getWorldCandidates() {
        Map<Date, String> worlds = new HashMap<Date, String>();
        File file = new File(Directories.SAVES);
        for (String s : file.list()) {
            File saveDir = new File(file.getAbsolutePath() + File.separator + s);
            if (isWorldDirectory(saveDir)) {
                WorldData data = WorldData.load(saveDir);
                if (data != null) {
                    Date lastModified = new Date(data.dataSave.lastModified());
                    worlds.put(lastModified, data.name);
                }
            }
        }
        return worlds;
    }

    public static boolean isWorldDirectory(File saveDir) {
        if (saveDir.listFiles() == null) {
            return false;
        }
        return Arrays.asList(saveDir.listFiles()).contains(new File(saveDir.getAbsolutePath() + File.separator + "data.cfg"));
    }
}
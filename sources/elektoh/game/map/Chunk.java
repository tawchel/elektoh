package elektoh.game.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import elektoh.game.entities.Entity;
import elektoh.game.entities.EntityRegistry;
import elektoh.game.tile.Tile;
import elektoh.game.utils.DataObject;
import elektoh.game.utils.GsonHelper;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.worldgen.TerrainType;

public class Chunk {
    private class Data {
        private Tile tile;
        private TerrainType terrain = TerrainType.OCEAN;
    }

    public static final int CHUNK_SIZE = 10;
    public final World world;
    private final Map<Coord, Data> tiles;
    private final List<Entity> entities;

    public Chunk(World worldObj) {
        world = worldObj;
        tiles = new HashMap<Coord, Data>();
        entities = new ArrayList<Entity>();
    }

    public void addEntity(Entity entity) {
        entities.add(entity);
    }

    public void setTerrainType(TerrainType terrain, Coord c) {
        if (!outOfBounds(c)) {
            get(c).terrain = terrain;
        }
    }

    public void setTile(Tile tile, Coord c) {
        if (!outOfBounds(c)) {
            get(c).tile = tile;
        }
    }

    public TerrainType getTerrainType(Coord c) {
        if (!outOfBounds(c)) {
            return get(c).terrain;
        }
        return null;
    }

    public Tile getTile(Coord c) {
        if (!outOfBounds(c)) {
            return get(c).tile;
        }
        return null;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public Map<Coord, Tile> getTiles() {
        HashMap<Coord, Tile> tmp = new HashMap<Coord, Tile>();
        for (Entry<Coord, Data> e : tiles.entrySet()) {
            tmp.put(e.getKey(), e.getValue().tile);
        }
        return tmp;
    }

    public Map<Coord, TerrainType> getTerrainTypes() {
        HashMap<Coord, TerrainType> tmp = new HashMap<Coord, TerrainType>();
        for (Entry<Coord, Data> e : tiles.entrySet()) {
            tmp.put(e.getKey(), e.getValue().terrain);
        }
        return tmp;
    }

    private boolean outOfBounds(Coord c) {
        return ((c.x < 0) || (c.y < 0) || (c.x > CHUNK_SIZE) || (c.y > CHUNK_SIZE));
    }

    private String getSaveString(Coord c) {
        return world.data.chunkDir.getAbsolutePath() + File.separator + c.x + "-" + c.y;
    }

    private Data get(Coord c) {
        if (tiles.get(c) == null) {
            tiles.put(c, new Data());
        }
        return tiles.get(c);
    }

    public void save(Coord c) {
        try {
            saveTiles(getSaveString(c));
            saveEntities(getSaveString(c));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveTiles(String chunk) throws IOException {
        if (!tiles.isEmpty()) {
            File save = new File(chunk + ".dat");
            FileWriter writer = new FileWriter(save);
            StringBuilder tmp = new StringBuilder();
            for (Entry<Coord, Data> e : tiles.entrySet()) {
                Data d = e.getValue();
                if (d.tile != null) {
                    tmp.append(e.getKey());
                    tmp.append(":");
                    tmp.append(world.data.dictionary.getTileID(d.tile));
                    tmp.append(",");
                    tmp.append(d.terrain.ordinal());
                    tmp.append(";");
                }
            }
            writer.write(tmp.toString());
            writer.close();
            save.length();
        }
    }

    private void saveEntities(String chunk) throws IOException {
        if (!getEntities().isEmpty()) {
            JsonArray root = new JsonArray();
            for (Entity entity : getEntities()) {
                JsonObject save = new JsonObject();
                entity.save(new DataObject(save));
                root.add(save);
            }
            File file = new File(chunk + "-entities.dat");
            GsonHelper.save(file, root);
        }
    }

    public void load(File chunk) {
        try {
            loadTiles(chunk);
            loadEntities(chunk);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadEntities(File chunk) throws Exception {
        String saveName = chunk.getName().replace(".dat", "-entities.dat");
        File save = new File(chunk.getParent() + File.separator + saveName);
        if (save.exists()) {
            JsonArray root = GsonHelper.parseArray(save);
            for (JsonElement element : root) {
                if (element != null) {
                    JsonObject obj = element.getAsJsonObject();
                    String name = obj.get("entityType").getAsString();
                    Entity entity = EntityRegistry.getEntity(name);
                    entity.load(new DataObject(obj));
                    addEntity(entity);
                }
            }
        }
    }

    private void loadTiles(File chunk) throws NumberFormatException, IOException {
        if (chunk.length() > 0) {
            BufferedReader reader = new BufferedReader(new FileReader(chunk));
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] instances = line.split(";");
                for (String instance : instances) {
                    String[] parts = instance.split(":");
                    String[] coords = parts[0].split(",");
                    String[] info = parts[1].split(",");
                    Coord c = new Coord(Integer.valueOf(coords[0]), Integer.valueOf(coords[1]));
                    setTile(world.data.dictionary.getTile(Integer.valueOf(info[0])), c);
                    setTerrainType(TerrainType.values()[Integer.valueOf(info[1])], c);
                }
            }
            reader.close();
        }
    }

    public static Coord getCoordFromFile(File chunk) {
        String[] coord = chunk.getName().replace(".dat", "").split("-");
        return new Coord(Integer.valueOf(coord[0]), Integer.valueOf(coord[1]));
    }
}
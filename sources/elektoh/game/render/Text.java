package elektoh.game.render;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.resource.FontHandler;
import elektoh.game.utils.Align;
import elektoh.game.utils.Color;
import elektoh.game.utils.Log;
import elektoh.game.utils.QuadSelector;
import elektoh.game.utils.geometry.Point;

public class Text {
    private Align horizontalAlign = Align.CENTER;
    private Align verticalAlign = Align.CENTER;
    private Color textColor = Color.BLACK;
    private Color shadowColor = null;
    private int defaultSize = 20;

    public void draw(String message, QuadSelector region) {
        int size = defaultSize;
        double stringWidth = FontHandler.FONT_TAWCHEL.getStringWidth(message, size);
        double stringHeight = FontHandler.FONT_TAWCHEL.getFontHeight(size);
        while (stringWidth > region.width) {
            size--;
            stringWidth = FontHandler.FONT_TAWCHEL.getStringWidth(message, size);
        }
        while (stringHeight > region.height) {
            size--;
            stringHeight = FontHandler.FONT_TAWCHEL.getFontHeight(size);
        }
        Point p = new Point(region.x, region.y);
        switch (horizontalAlign) {
            case CENTER:
                p = p.translate(region.width / 2, 0);
                break;
            case RIGHT:
                p = p.translate(region.width, 0);
                break;
            case BOTTOM:
            case TOP:
                Log.warning("BOTTOM / TOP cannot be used for horizontal align when rendering " + message);
                return;
            case LEFT:
            default:
                break;
        }
        switch (verticalAlign) {
            case CENTER:
                p = p.translate(0, region.height / 2);
                break;
            case BOTTOM:
                p = p.translate(0, region.height);
                break;
            case LEFT:
            case RIGHT:
                Log.warning("LEFT / RIGHT cannot be used for vertical align when rendering " + message);
                return;
            case TOP:
            default:
                break;
        }
        draw(message, p, size);
    }

    public void draw(String message, Point p) {
        draw(message, p, defaultSize);
    }

    private void draw(String message, Point p, int size) {
        glPushMatrix();
        glTranslated(p.x, p.y, 0);
        double stringWidth = FontHandler.FONT_TAWCHEL.getStringWidth(message, size);
        double stringHeight = FontHandler.FONT_TAWCHEL.getFontHeight(size);
        switch (horizontalAlign) {
            case CENTER:
                glTranslated(-(stringWidth / 2), 0, 0);
                break;
            case RIGHT:
                glTranslated(-stringWidth, 0, 0);
                break;
            case BOTTOM:
            case TOP:
                Log.warning("BOTTOM / TOP cannot be used for horizontal align when rendering " + message);
                return;
            case LEFT:
            default:
                break;
        }
        switch (verticalAlign) {
            case CENTER:
                glTranslated(0, -(stringHeight / 2), 0);
                break;
            case BOTTOM:
                glTranslated(0, -stringHeight, 0);
                break;
            case LEFT:
            case RIGHT:
                Log.warning("LEFT / RIGHT cannot be used for vertical align when rendering " + message);
                return;
            case TOP:
            default:
                break;
        }
        if (shadowColor != null) {
            glPushMatrix();
            glTranslated(0, 1, 0);
            FontHandler.FONT_TAWCHEL.drawString(message, size, shadowColor);
            glPopMatrix();
        }
        FontHandler.FONT_TAWCHEL.drawString(message, size, textColor);
        glPopMatrix();
    }

    public Align getHorizontalAlign() {
        return horizontalAlign;
    }

    public Align getVerticalAlign() {
        return verticalAlign;
    }

    public Color getTextColor() {
        return textColor;
    }

    public Color getShadowColor() {
        return shadowColor;
    }

    public int getSize() {
        return defaultSize;
    }

    public void setAlign(Align horizontalAlign, Align verticalAlign) {
        this.horizontalAlign = horizontalAlign;
        this.verticalAlign = verticalAlign;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public void setShadowColor(Color shadowColor) {
        this.shadowColor = shadowColor;
    }

    public void setSize(int defaultSize) {
        this.defaultSize = defaultSize;
    }
}
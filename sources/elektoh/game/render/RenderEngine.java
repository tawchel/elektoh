package elektoh.game.render;

import static java.lang.Math.*;
import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

import elektoh.game.Elektoh;
import elektoh.game.entities.Entity;
import elektoh.game.map.World;
import elektoh.game.resource.sprite.Sprite;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.tile.Tile;
import elektoh.game.tile.TileRegistry;
import elektoh.game.tile.Tiles;
import elektoh.game.utils.Color;
import elektoh.game.utils.Direction;
import elektoh.game.utils.MathUtils;
import elektoh.game.utils.QuadSelector;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Point;

public class RenderEngine {
    public static final int XY_TILE_WIDTH = 50;
    public static final int XY_TILE_HEIGHT = 26;
    public static final double XY_TILE_WIDTH_HALF = XY_TILE_WIDTH / 2d;
    public static final double XY_TILE_HEIGHT_HALF = XY_TILE_HEIGHT / 2d;
    public static final double XY_TILE_DIAGONAL = sqrt(pow(XY_TILE_WIDTH_HALF, 2) + pow(XY_TILE_HEIGHT_HALF, 2));
    public final World world;
    public double iso_cameraX = 0;
    public double iso_cameraY = 0;
    public double zoom = 1;

    public RenderEngine(World world) {
        this.world = world;
    }

    public void renderWorld() {
        glPushMatrix();
        glTranslated((Elektoh.WINDOW_DEF_X / 2d), (Elektoh.WINDOW_DEF_Y / 2d), 0);
        glScaled(4 * zoom, 4 * zoom, 0);
        renderTiles();
        renderEntities();
        glPopMatrix();
    }

    public QuadSelector getRenderWindow() {
        double d1 = ((Elektoh.WINDOW_DEF_X / (4d * zoom)) * sin(toRadians(15))) / sin(toRadians(30));
        double screenDiagonal = (Elektoh.WINDOW_DEF_X / (4d * zoom)) + d1;
        int range = (int) round(screenDiagonal / XY_TILE_DIAGONAL / 2);
        int xMin = (int) (round(iso_cameraX) - range);
        int yMin = (int) (round(iso_cameraY) - range);
        return new QuadSelector(xMin, yMin, range * 2, range * 2);
    }

    private void renderTiles() {
        QuadSelector region = getRenderWindow();
        for (int x = MathUtils.round(region.x); x < region.x + region.width; x++) {
            for (int y = MathUtils.round(region.y); y < region.y + region.height; y++) {
                renderTile(x, y, 0);
            }
        }
        for (int x = MathUtils.round(region.x); x < region.x + region.width; x++) {
            for (int y = MathUtils.round(region.y); y < region.y + region.height; y++) {
                renderTile(x, y, 1);
            }
        }
    }

    private void renderTile(int x, int y, int pass) {
        glPushMatrix();
        iso_translate(new Point(x, y));
        glTranslated(-XY_TILE_WIDTH_HALF, -XY_TILE_HEIGHT_HALF, 0);
        Tile currTile = world.getTile(x, y);
        if (currTile == null) {
            currTile = Tiles.ocean;
        }
        if (pass == 0) {
            Sprite sprite = SpriteDatabase.getSprite("tiles/" + currTile.name);
            Color color = currTile.getColor(world, x, y);
            if (color == null) {
                color = Color.WHITE;
            }
            sprite.render(color);
        }
        if (pass == 1) {
            for (Tile tile : TileRegistry.getAllTiles()) {
                RenderHelper.renderConnectedTextures(this, currTile, tile, x, y);
            }
            // RenderHelper.renderVariations(this, currTile, x, y);
            if (Elektoh.DEV_COORDS) {
                if (zoom >= 1) {
                    Text text = new Text();
                    text.setTextColor(Color.WHITE);
                    text.draw(x + "," + y, new QuadSelector(0, 0, XY_TILE_WIDTH, XY_TILE_HEIGHT));
                }
            }
        }
        glPopMatrix();
    }

    private void renderEntities() {
        glPushMatrix();
        Coord cameraChunk = world.getChunkCoord(new Coord((int) round(iso_cameraX), (int) round(iso_cameraY)));
        List<Entity> entities = new ArrayList<Entity>();
        List<Entity> orderedEntities = new ArrayList<Entity>();
        for (int i = 0; i < Direction.values().length + 1; i++) {
            Coord chunkCoord;
            if (i < Direction.values().length) {
                Direction d = Direction.values()[i];
                chunkCoord = new Coord(cameraChunk.x + d.x, cameraChunk.y + d.y);
            } else {
                chunkCoord = cameraChunk;
            }
            entities.addAll(world.getChunk(chunkCoord).getEntities());
        }
        while (!entities.isEmpty()) {
            Entity next = entities.get(0);
            for (Entity entity : entities) {
                if (next.getXYLayer() > entity.getXYLayer()) {
                    next = entity;
                }
            }
            orderedEntities.add(next);
            entities.remove(next);
        }
        for (Entity entity : orderedEntities) {
            glPushMatrix();
            iso_translate(entity.getPosition());
            entity.render(this);
            glPopMatrix();
        }
        glPopMatrix();
    }

    public void iso_translate(Point p) {
        double xDiff = iso_cameraX - p.x;
        double yDiff = iso_cameraY - p.y;
        iso_translateRelative(xDiff, yDiff);
    }

    public void iso_translateRelative(double xDiff, double yDiff) {
        glTranslated(-(XY_TILE_WIDTH_HALF + 1.0d) * xDiff, -(XY_TILE_HEIGHT_HALF) * xDiff, 0);
        glTranslated(+(XY_TILE_WIDTH_HALF + 1.0d) * yDiff, -(XY_TILE_HEIGHT_HALF) * yDiff, 0);
    }
}
package elektoh.game.render;

import static org.lwjgl.opengl.GL11.*;

import java.util.Random;

import elektoh.game.resource.sprite.Sprite;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.tile.Tile;
import elektoh.game.utils.Direction;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Edge;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;

public class RenderHelper {
    public static void renderConnectedTextures(RenderEngine engine, Tile tile, Tile targetTile, int x, int y) {
        String[] ctCodes = new String[2];
        String current = null;
        for (Direction d : new Direction[] { Direction.WEST, Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST, Direction.NORTH }) {
            Tile currTile = engine.world.getTile(x + d.iso_x(), y + d.iso_y());
            if ((currTile != null) && (currTile == targetTile) && (currTile.renderLayer > tile.renderLayer)) {
                if (current == null) {
                    current = currTile.name + ".CT.";
                }
                if (!current.isEmpty() && !current.endsWith(".")) {
                    current += "&";
                }
                current += d.opposite().abbrv;
            } else if (current != null) {
                if (ctCodes[0] == null) {
                    ctCodes[0] = current;
                } else {
                    if (!ctCodes[0].equals(current)) {
                        if (current.length() > ctCodes[0].length()) {
                            ctCodes[0] = current;
                        }
                        if (current.length() == ctCodes[0].length()) {
                            ctCodes[1] = current;
                        }
                    }
                }
                current = null;
            }
        }
        if (current != null) {
            if (ctCodes[0] == null) {
                ctCodes[0] = current;
            } else {
                if (!ctCodes[0].equals(current)) {
                    if (current.length() > ctCodes[0].length()) {
                        ctCodes[0] = current;
                    }
                    if (current.length() == ctCodes[0].length()) {
                        ctCodes[1] = current;
                    }
                }
            }
        }
        for (String ctCode : ctCodes) {
            if (ctCode != null) {
                Sprite sprite = SpriteDatabase.getSprite("tiles/" + ctCode);
                if (sprite != null) {
                    sprite.render();
                }
            }
        }
        for (Direction d : Direction.SECONDARY) {
            Tile currTile = engine.world.getTile(x + d.iso_x(), y + d.iso_y());
            if ((currTile != null) && (currTile == targetTile) && (currTile.renderLayer > tile.renderLayer)) {
                Tile t1 = engine.world.getTile(x + d.iso_x(), y);
                Tile t2 = engine.world.getTile(x, y + d.iso_y());
                if ((t1 != null) && (t2 != null) && (currTile.renderLayer > t1.renderLayer) && (currTile.renderLayer > t2.renderLayer)) {
                    Sprite sprite = SpriteDatabase.getSprite("tiles/" + currTile.name + ".CT." + d.opposite().abbrv);
                    if (sprite != null) {
                        sprite.render();
                    }
                }
            }
        }
    }

    public static void renderVariations(RenderEngine engine, Tile tile, int x, int y) {
        Random rand = new Random(engine.world.data.name.hashCode() + new Coord(x, y).hashCode());
        double randVal = rand.nextInt(10);
        if (randVal == 0) {
            glPushMatrix();
            double deviation = 0.5d;
            engine.iso_translateRelative(Math.round((rand.nextDouble() - deviation) * deviation), Math.round((rand.nextDouble() - deviation) * deviation));
            // Sprite sprite = ResourceManager.tileSheet.getSprite(tile.name + ".variation." + rand.nextInt(tile.variations));
            // sprite.render();
            glPopMatrix();
        }
    }

    public static void draw(Polygon poly) {
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_POLYGON);
        for (Point p : poly.getPoints()) {
            glVertex2d(p.x, p.y);
        }
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }

    public static void draw(Edge edge) {
        glDisable(GL_TEXTURE_2D);
        glBegin(GL_LINES);
        glVertex3d(edge.start.x, edge.start.y, 0);
        glVertex3d(edge.finish.x, edge.finish.y, 0);
        glEnd();
        glEnable(GL_TEXTURE_2D);
    }
}
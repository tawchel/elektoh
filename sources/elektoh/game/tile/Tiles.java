package elektoh.game.tile;

public class Tiles {
    public static Tile grass;
    public static Tile stone;
    public static Tile sand;
    public static Tile ocean;

    public static void load() {
        grass = TileRegistry.addTile(new Tile("grass"));
        stone = TileRegistry.addTile(new Tile("stone"));
        sand = TileRegistry.addTile(new Tile("sand"));
        ocean = TileRegistry.addTile(new Tile("ocean", false));
    }
}
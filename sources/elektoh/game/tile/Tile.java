package elektoh.game.tile;

import elektoh.game.map.World;
import elektoh.game.utils.Color;
import elektoh.game.utils.Direction;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.worldgen.TerrainType;

public class Tile {
    private static int currLayer = 0;
    public final String name;
    public final int renderLayer;
    public final boolean walkable;

    public Tile(String name) {
        this(name, true);
    }

    public Tile(String name, boolean walkable) {
        this.name = name;
        this.walkable = walkable;
        renderLayer = currLayer;
        currLayer++;
    }

    public Color getColor(World world, int x, int y) {
        if ((this == Tiles.grass) && (world.getTerrainType(new Coord(x, y)) != null)) {
            TerrainType thisTerrain = world.getTerrainType(new Coord(x, y));
            Color color = thisTerrain.grassColor();
            for (Direction d : Direction.values()) {
                TerrainType currTerrain = world.getTerrainType(new Coord(x + d.iso_x(), y + d.iso_y()));
                if ((currTerrain != null) && (currTerrain != thisTerrain) && (currTerrain.grassColor() != null)) {
                    if (color != null) {
                        color = color.mix(currTerrain.grassColor());
                    } else {
                        color = currTerrain.grassColor();
                    }
                }
            }
            if (thisTerrain.grassColor() != null) {
                color = color.mix(thisTerrain.grassColor());
                Color tempColor = color.mix(thisTerrain.grassColor());
                color = color.mix(tempColor);
            }
            // return color;
            return null;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Tile [name=").append(name).append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        result = (prime * result) + renderLayer;
        result = (prime * result) + (walkable ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Tile other = (Tile) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (renderLayer != other.renderLayer) {
            return false;
        }
        if (walkable != other.walkable) {
            return false;
        }
        return true;
    }
}
package elektoh.game.tile;

import java.util.ArrayList;
import java.util.List;

public class TileRegistry {
    private static List<Tile> tiles = new ArrayList<Tile>();

    public static Tile addTile(Tile tile) {
        tiles.add(tile);
        return tile;
    }

    public static Tile getTile(String name) {
        for (Tile tile : tiles) {
            if (tile.name.equalsIgnoreCase(name)) {
                return tile;
            }
        }
        return null;
    }

    public static List<Tile> getAllTiles() {
        return tiles;
    }
}
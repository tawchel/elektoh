package elektoh.game.savedata;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import elektoh.game.tile.Tile;
import elektoh.game.tile.TileRegistry;

public class TileDictionary {
    private Map<Integer, String> ids;
    private int nextID = 0;

    public TileDictionary() {
        ids = new HashMap<Integer, String>();
    }

    public boolean containsTile(Tile tile) {
        return ids.containsValue(tile.name);
    }

    public Tile getTile(int id) {
        return TileRegistry.getTile(ids.get(id));
    }

    public int addTile(Tile tile) {
        int id = nextID++;
        ids.put(id, tile.name);
        return id;
    }

    public int getTileID(Tile tile) {
        for (Entry<Integer, String> e : ids.entrySet()) {
            if (e.getValue().equalsIgnoreCase(tile.name)) {
                return e.getKey();
            }
        }
        return addTile(tile);
    }
}
package elektoh.game.logic;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.Elektoh;
import elektoh.game.gui.Button;
import elektoh.game.gui.Scrollbar;
import elektoh.game.io.types.MouseInput;
import elektoh.game.render.RenderHelper;
import elektoh.game.utils.Color;

public class SettingsLogic extends GuiLogic {
    public Scrollbar scrollbar;

    public SettingsLogic(ILogic oneUp) {
        super(oneUp);
    }

    @Override
    public void open() {
        super.open();
        elementList.add(new Button("cancel", grid.getRegion(7.5d, 5, 1.5d, 1)));
        scrollbar = new Scrollbar("settings", grid.getRegion(1, 1, 8, 4, 0));
    }

    @Override
    public boolean update() {
        super.update();
        int mouseX = (int) Math.round(MouseInput.mouseX / 4D);
        int mouseY = (int) Math.round(MouseInput.mouseY / 4D);
        scrollbar.update(mouseX, mouseY);
        return true;
    }

    @Override
    public void render() {
        glPushMatrix();
        glScaled(4, 4, 0);
        scrollbar.render();
        Color.WHITE.bind();
        RenderHelper.draw(grid.getRegion(1, 5, 8, 1, 0).toPolygon());
        elementList.render();
        if (Elektoh.DEV_ART) {
            grid.render();
        }
        glPopMatrix();
    }
}
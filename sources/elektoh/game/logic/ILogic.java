package elektoh.game.logic;

public interface ILogic {
    public void open();

    public boolean update();

    public void render();

    public void close();
}
package elektoh.game.logic;

import static org.lwjgl.opengl.GL11.*;

import elektoh.game.Elektoh;
import elektoh.game.gui.Grid;
import elektoh.game.gui.GuiElementList;
import elektoh.game.gui.Selector;
import elektoh.game.io.types.MouseInput;

public abstract class GuiLogic implements ILogic {
    public static Grid grid = new Grid(8, 5, 4);
    protected GuiElementList elementList;
    protected ILogic oneUp;

    public GuiLogic(ILogic oneUp) {
        this.oneUp = oneUp;
    }

    @Override
    public void open() {
        elementList = new GuiElementList();
    }

    @Override
    public boolean update() {
        if (MouseInput.mouseLeftClick_single) {
            Selector.unselect();
        }
        int mouseX = (int) Math.round(MouseInput.mouseX / 4D);
        int mouseY = (int) Math.round(MouseInput.mouseY / 4D);
        elementList.update(mouseX, mouseY);
        if (elementList.getButtonClicked("cancel") && (oneUp != null)) {
            Elektoh.setCurrentLogic(oneUp);
        }
        return true;
    }

    @Override
    public void render() {
        glPushMatrix();
        glScaled(4, 4, 0);
        elementList.render();
        if (Elektoh.DEV_ART) {
            grid.render();
        }
        glPopMatrix();
    }

    @Override
    public void close() {
        Selector.unselect();
    }
}
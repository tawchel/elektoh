package elektoh.game.logic;

import elektoh.game.Elektoh;
import elektoh.game.gui.Button;
import elektoh.game.settings.SettingsHandler;

public class Menu extends GuiLogic {
    public Menu(int key) {
        super(null);
    }

    @Override
    public void open() {
        super.open();
        elementList.add(new Button("play", grid.getRegion(1, 2, 3, 1), true));
        elementList.add(new Button("load", grid.getRegion(1, 3, 3, 1), SettingsHandler.settings.first_launch));
        elementList.add(new Button("options", grid.getRegion(1, 4, 3, 1)));
        elementList.add(new Button("quit", grid.getRegion(1, 5, 2, 1)));
    }

    @Override
    public boolean update() {
        super.update();
        if (elementList.getButtonClicked("quit")) {
            return false;
        }
        if (elementList.getButtonClicked("load")) {
            Elektoh.setCurrentLogic(Elektoh.LOAD_WORLD);
        }
        if (elementList.getButtonClicked("options")) {
            Elektoh.setCurrentLogic(new SettingsLogic(this));
        }
        return true;
    }
}
package elektoh.game.logic;

import elektoh.game.Elektoh;
import elektoh.game.gui.Button;
import elektoh.game.gui.StringInput;
import elektoh.game.map.World;

public class NewWorld extends GuiLogic {
    public NewWorld(int key) {
        super(Elektoh.LOAD_WORLD);
    }

    @Override
    public void open() {
        super.open();
        elementList.add(new Button("create", grid.getRegion(1, 5, 2, 1)));
        elementList.add(new Button("cancel", grid.getRegion(7.5d, 5, 1.5d, 1)));
        elementList.add(new StringInput("worldName", "Elektoh", true, 20, grid.getRegion(2, 2, 6, 1)));
    }

    @Override
    public boolean update() {
        super.update();
        StringInput worldName = (StringInput) elementList.getElement("worldName");
        Button create = (Button) elementList.getElement("create");
        create.disabled = worldName.getOutput().equals("") || World.getWorldCandidates().containsValue(worldName.getOutput());
        if (elementList.getButtonClicked("create")) {
            World world = World.create(worldName.getOutput());
            Elektoh.setCurrentLogic(new Play(world));
        }
        return true;
    }
}
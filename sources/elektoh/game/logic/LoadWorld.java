package elektoh.game.logic;

import static org.lwjgl.opengl.GL11.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import elektoh.game.Elektoh;
import elektoh.game.gui.Button;
import elektoh.game.gui.Scrollbar;
import elektoh.game.gui.Selector;
import elektoh.game.gui.WorldSelector;
import elektoh.game.io.types.MouseInput;
import elektoh.game.map.World;
import elektoh.game.render.RenderHelper;
import elektoh.game.utils.Color;

public class LoadWorld extends GuiLogic {
    public List<World> worlds;
    public Scrollbar scrollbar;

    public LoadWorld(int key) {
        super(Elektoh.MENU);
    }

    @Override
    public void open() {
        super.open();
        elementList.add(new Button("select", grid.getRegion(3, 5, 2, 1), true));
        elementList.add(new Button("new", grid.getRegion(5, 5, 2, 1)));
        elementList.add(new Button("cancel", grid.getRegion(7.5d, 5, 1.5d, 1)));
        Map<Date, String> worlds = World.getWorldCandidates();
        scrollbar = new Scrollbar("worldSelector", grid.getRegion(2, 1, 6, 4, 0));
        int index = 0;
        while (!worlds.isEmpty()) {
            Entry<Date, String> oldest = null;
            for (Entry<Date, String> entry : worlds.entrySet()) {
                if (oldest != null) {
                    if (oldest.getKey().before(entry.getKey())) {
                        oldest = entry;
                    }
                } else {
                    oldest = entry;
                }
            }
            scrollbar.add(new WorldSelector(oldest.getValue(), scrollbar.grid, index));
            worlds.remove(oldest.getKey());
            index++;
        }
        if (!scrollbar.elements.isEmpty()) {
            Selector.disableUnselect();
        }
    }

    @Override
    public boolean update() {
        super.update();
        int mouseX = (int) Math.round(MouseInput.mouseX / 4D);
        int mouseY = (int) Math.round(MouseInput.mouseY / 4D);
        scrollbar.update(mouseX, mouseY);
        if (elementList.getButtonClicked("new")) {
            Elektoh.setCurrentLogic(Elektoh.NEW_WORLD);
        }
        if (Selector.getSelected() instanceof WorldSelector) {
            elementList.getElement("select").disabled = false;
            if (elementList.getButtonClicked("select")) {
                WorldSelector worldSelector = (WorldSelector) Selector.getSelected();
                String worldName = worldSelector.name;
                World world = World.load(World.getWorldDirFromName(worldName));
                Elektoh.setCurrentLogic(new Play(world));
            }
        } else {
            elementList.getElement("select").disabled = true;
        }
        return true;
    }

    @Override
    public void render() {
        glPushMatrix();
        glScaled(4, 4, 0);
        scrollbar.render();
        Color.WHITE.bind();
        RenderHelper.draw(grid.getRegion(1, 5, 8, 1, 0).toPolygon());
        elementList.render();
        if (Elektoh.DEV_ART) {
            grid.render();
        }
        glPopMatrix();
    }

    @Override
    public void close() {
        super.close();
        Selector.enableUnselect();
    }
}
package elektoh.game.logic;

import static org.lwjgl.opengl.GL11.*;
import elektoh.game.Elektoh;
import elektoh.game.io.Camara;
import elektoh.game.io.PlayerInput;
import elektoh.game.io.types.KeyboardInput;
import elektoh.game.io.types.MouseInput;
import elektoh.game.map.World;
import elektoh.game.render.RenderEngine;
import elektoh.game.settings.SettingsHandler;
import elektoh.game.utils.Log;
import elektoh.game.utils.MathUtils;

public class Play implements ILogic {
    public final World world;
    public RenderEngine renderEngine;

    public Play(World world) {
        this.world = world;
    }

    @Override
    public void open() {
        renderEngine = new RenderEngine(world);
        PlayerInput.active = true;
    }

    @Override
    public boolean update() {
        long l = System.currentTimeMillis();
        world.update();
        if (KeyboardInput.isKeyDown_single(SettingsHandler.settings.keyBinds.crudeSave)) {
            world.save();
        }
        long time = System.currentTimeMillis() - l;
        if (time >= 50) {
            Log.warning("The previous tick took " + time + " milliseconds!");
        }
        if (time >= 60) {
            Log.severe("The game is taking longer to tick than the screen takes to update!! This is BAD!");
        }
        return true;
    }

    @Override
    public void render() {
        handleCamera();
        renderEngine.renderWorld();
        renderOverlay();
    }

    private void handleCamera() {
        Camara camara = world.getCamara();
        if (!Elektoh.DEV_MODE) {
            renderEngine.iso_cameraX = camara.getPosition().x - (camara.getHeight() / 3);
            renderEngine.iso_cameraY = camara.getPosition().y - (camara.getHeight() / 3);
            renderEngine.zoom = 1;
        }
        if (Elektoh.DEV_MODE) {
            if (MouseInput.mouseLeftClick) {
                renderEngine.iso_cameraX -= MouseInput.mouseSpeedX / (100d * renderEngine.zoom);
                renderEngine.iso_cameraY += MouseInput.mouseSpeedX / (100d * renderEngine.zoom);
                renderEngine.iso_cameraX += MouseInput.mouseSpeedY / (100d * renderEngine.zoom);
                renderEngine.iso_cameraY += MouseInput.mouseSpeedY / (100d * renderEngine.zoom);
                renderEngine.iso_cameraX = MathUtils.minMax(renderEngine.iso_cameraX, 0, world.data.tileSize);
                renderEngine.iso_cameraY = MathUtils.minMax(renderEngine.iso_cameraY, 0, world.data.tileSize);
            }
            renderEngine.zoom += MouseInput.scrollSpeed / 500d;
            renderEngine.zoom = MathUtils.minMax(renderEngine.zoom, 0.1d, 2);
        }
    }

    private void renderOverlay() {
        glPushMatrix();
        glScaled(4, 4, 0);
        // TODO
        glPopMatrix();
    }

    @Override
    public void close() {
        world.save();
        PlayerInput.active = false;
    }
}
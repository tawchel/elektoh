package elektoh.game;

import static org.lwjgl.opengl.GL11.*;

import java.io.File;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import elektoh.game.entities.EntityRegistry;
import elektoh.game.io.Input;
import elektoh.game.io.Window;
import elektoh.game.io.types.KeyboardInput;
import elektoh.game.io.types.MouseInput;
import elektoh.game.logic.ILogic;
import elektoh.game.logic.LoadWorld;
import elektoh.game.logic.Menu;
import elektoh.game.logic.NewWorld;
import elektoh.game.resource.ResourceManager;
import elektoh.game.resource.sprite.SpriteAnimated;
import elektoh.game.settings.SettingsHandler;
import elektoh.game.tile.Tiles;
import elektoh.game.utils.Directories;
import elektoh.game.utils.Log;
import elektoh.game.utils.geometry.Point;
import elektoh.game.worldgen.WorldGenerator;
import elektoh.game.worldgen.populate.PopulatorDecor;

/**
 * Before we begin, we must put ourselves into the right mindset.
 * <prayer>
 * Our father in heaven
 * hallowed be thy name
 * Your project come,
 * your code be done,
 * in Java, and in Objective-C
 * Give us this day our daily caffeine,
 * and forgive us our bugs,
 * as we have also forgiven our IDE
 * And lead us not to error
 * but deliver us from messy code.
 * For thine is the source code,
 * and the compiler, and the interpreter,
 * for ever.
 * Amen.
 * </prayer>
 */
public class Elektoh {
    public static final int WINDOW_DEF_X = 1048;
    public static final int WINDOW_DEF_Y = 640;
    public static final double WINDOW_DEF_RATIO = (double) WINDOW_DEF_X / (double) WINDOW_DEF_Y;
    public static final Point WINDOW_DEF_CENTER = new Point(WINDOW_DEF_X / 2d, WINDOW_DEF_Y / 2d);
    public static final boolean DEV_ART = false;
    public static final boolean DEV_MODE = false;
    public static final boolean DEV_COORDS = false;
    public static boolean running;
    private static File directory;
    private static ILogic currentLogic;
    private static int gameTime = 0;
    public static final ILogic MENU = new Menu(0);
    public static final ILogic LOAD_WORLD = new LoadWorld(0);
    public static final ILogic NEW_WORLD = new NewWorld(0);

    public static void create(File dir) {
        directory = dir;
        Log.create();
        Directories.make();
        SettingsHandler.initialize();
        Window.create("Elektoh", WINDOW_DEF_X, WINDOW_DEF_Y, true);
        Input.addType(0, new MouseInput());
        Input.addType(1, new KeyboardInput());
        ResourceManager.loadResources();
        Tiles.load();
        setCurrentLogic(MENU);
        EntityRegistry.registerEntities();
        addPopulators();
        run();
    }

    // TODO move this
    private static void addPopulators() {
        WorldGenerator.addPopulator(new PopulatorDecor());
    }

    public static void setGameDirectory(File dir) {
        directory = dir;
    }

    public static File getGameDir() {
        return directory;
    }

    public static void run() {
        running = true;
        while (running) {
            if (gameTime == Integer.MAX_VALUE) {
                gameTime = 0;
            }
            gameTime++;
            Input.update();
            running = currentLogic.update();
            SpriteAnimated.update(gameTime);
            if (running) {
                running = Window.update();
            }
            glClear(GL_COLOR_BUFFER_BIT);
            glPushMatrix();
            currentLogic.render();
            glPopMatrix();
            if (KeyboardInput.isKeyDown_single(SettingsHandler.settings.keyBinds.reloadResourcesKey)) {
                ResourceManager.loadResources();
            }
        }
        exit();
    }

    public static void setCurrentLogic(ILogic logic) {
        if (currentLogic != null) {
            currentLogic.close();
        }
        currentLogic = logic;
        currentLogic.open();
    }

    public static int getGameTime() {
        return gameTime;
    }

    public static void exit() {
        SettingsHandler.settings.first_launch = false;
        SettingsHandler.save();
        currentLogic.close();
        Log.info("Exiting...");
        Display.destroy();
        Mouse.destroy();
        System.exit(0);
    }
}
package elektoh.game.io;

import elektoh.game.utils.geometry.Point;

public interface Camara {
    public Point getPosition();

    public double getHeight();
}
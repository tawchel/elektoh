package elektoh.game.io;

import java.util.HashMap;
import java.util.Map;

import elektoh.game.io.types.InputType;
import elektoh.game.settings.SettingsHandler;

public class Input {
    private static Map<Integer, InputType> inputTypes = new HashMap<Integer, InputType>();

    public static void update() {
        for (InputType type : inputTypes.values()) {
            type.update();
        }
        if (PlayerInput.active) {
            inputTypes.get(SettingsHandler.settings.input).updatePlayer();
            PlayerInput.update();
        }
    }

    public static void addType(int id, InputType type) {
        if (type != null) {
            if (!inputTypes.containsKey(id)) {
                inputTypes.put(id, type);
                type.create();
            }
        }
    }
}
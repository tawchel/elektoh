package elektoh.game.io.types;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import elektoh.game.Elektoh;
import elektoh.game.entities.Speed;
import elektoh.game.io.PlayerInput;
import elektoh.game.io.Window;
import elektoh.game.settings.SettingsHandler;
import elektoh.game.utils.Direction;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Vector;

public class MouseInput implements InputType {
    public static int mouseX;
    public static int mouseY;
    public static int mouseSpeedX;
    public static int mouseSpeedY;
    public static int scrollSpeed;
    public static boolean movementClick;
    public static boolean mouseLeftClick;
    public static boolean mouseRightClick;
    public static boolean mouseLeftClick_single;
    public static boolean mouseRightClick_single;

    @Override
    public void create() {
        try {
            Mouse.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update() {
        Coord mouse = Window.calibratePointer(new Coord(Mouse.getX(), Display.getHeight() - Mouse.getY()));
        mouseX = mouse.x;
        mouseY = mouse.y;
        mouseSpeedX = Mouse.getDX();
        mouseSpeedY = Mouse.getDY();
        scrollSpeed = Mouse.getDWheel();
        mouseLeftClick_single = !mouseLeftClick && Mouse.isButtonDown(0);
        mouseRightClick_single = !mouseRightClick && Mouse.isButtonDown(1);
        movementClick = Mouse.isButtonDown(SettingsHandler.settings.keyBinds.mouseMovement);
        mouseLeftClick = Mouse.isButtonDown(0);
        mouseRightClick = Mouse.isButtonDown(1);
    }

    @Override
    public void updatePlayer() {
        Vector mouseVector = Vector.createStandard(new Point(Elektoh.WINDOW_DEF_X / 2, Elektoh.WINDOW_DEF_Y / 2), new Point(MouseInput.mouseX, MouseInput.mouseY));
        PlayerInput.facing = Direction.getDirectionFromRotation(mouseVector.angle()).xyToIso();
        PlayerInput.moving = MouseInput.movementClick;
        if (mouseVector.magnitude() < 130) {
            PlayerInput.speed = Speed.SNEAK;
        } else {
            PlayerInput.speed = Speed.WALK;
        }
    }

    @Override
    public String getName() {
        return "mouse";
    }
}
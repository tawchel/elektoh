package elektoh.game.io.types;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;

import elektoh.game.Elektoh;
import elektoh.game.entities.Speed;
import elektoh.game.io.PlayerInput;
import elektoh.game.settings.SettingsHandler;
import elektoh.game.utils.Direction;

public class KeyboardInput implements InputType {
    private static Map<Integer, Integer> keyHoldTimes;
    public static String currentString = "";

    @Override
    public void create() {
        try {
            Keyboard.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
        keyHoldTimes = new HashMap<Integer, Integer>();
    }

    @Override
    public void update() {
        for (int key : keyHoldTimes.keySet()) {
            keyHoldTimes.put(key, keyHoldTimes.get(key) + 1);
        }
        currentString = "";
        while (Keyboard.next()) {
            int key = Keyboard.getEventKey();
            boolean pressed = Keyboard.getEventKeyState();
            char c = Keyboard.getEventCharacter();
            if (!pressed) {
                keyHoldTimes.remove(key);
            } else {
                if (c != Keyboard.CHAR_NONE) {
                    currentString += c;
                }
                keyHoldTimes.put(key, 0);
            }
        }
    }

    @Override
    public void updatePlayer() {
        Direction d = Direction.getDirectionFromKeys();
        if (d == null) {
            PlayerInput.moving = false;
        } else {
            PlayerInput.moving = true;
            PlayerInput.facing = d.xyToIso();
        }
        if (isKeyDown(SettingsHandler.settings.keyBinds.sneakKey)) {
            PlayerInput.speed = Speed.SNEAK;
        } else {
            PlayerInput.speed = Speed.WALK;
        }
    }

    public static boolean isKeyDown(String key) {
        key = keyTranslate(key);
        return isKeyDown(Keyboard.getKeyIndex(key.toUpperCase()));
    }

    public static boolean isKeyDown(int key) {
        return keyHoldTimes.containsKey(key);
    }

    public static boolean isKeyDown_single(String key) {
        key = keyTranslate(key);
        return isKeyDown_single(Keyboard.getKeyIndex(key.toUpperCase()));
    }

    public static boolean isKeyDown_single(int key) {
        return keyHoldTimes.containsKey(key) && (keyHoldTimes.get(key) == 0);
    }

    public static int getKeyHeldTime(String key) {
        key = keyTranslate(key);
        return getKeyHeldTime(Keyboard.getKeyIndex(key.toUpperCase()));
    }

    public static int getKeyHeldTime(int key) {
        return keyHoldTimes.containsKey(key) ? keyHoldTimes.get(key) : -1;
    }

    private static String keyTranslate(String key) {
        if (key.equals("backspace")) {
            return "back";
        }
        return key;
    }

    public static String getStringFromTyping(String prevString) {
        String string = prevString;
        if (!currentString.isEmpty()) {
            string += currentString;
        }
        if (isKeyDown_single("backspace") || ((getKeyHeldTime("backspace") > 40) && ((Elektoh.getGameTime() % 5) == 0))) {
            if (string.length() >= 2) {
                string = string.subSequence(0, string.length() - 2).toString();
            } else {
                string = "";
            }
        }
        return string;
    }

    public static String blinky() {
        if ((Elektoh.getGameTime() % 100) >= 50) {
            return "|";
        }
        return "";
    }

    @Override
    public String getName() {
        return "keyboard";
    }
}
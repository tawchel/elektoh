package elektoh.game.io.types;

public interface InputType {
    public void create();

    public void update();

    public void updatePlayer();

    public String getName();
}
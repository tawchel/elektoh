package elektoh.game.io;

import static java.lang.Math.*;
import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import elektoh.game.Elektoh;
import elektoh.game.utils.geometry.Coord;

public class Window {
    public static int width;
    public static int height;
    public static boolean resized = false;

    public static void create(String windowName, int x, int y, boolean resizeable) {
        width = x;
        height = y;
        try {
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
        setDisplayMode(x, y, false);
        Display.setTitle(windowName);
        Display.setResizable(resizeable);
        openGLinit();
    }

    private static void openGLinit() {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, width, height, 0, 1, -1);
        glMatrixMode(GL_MODELVIEW);
        glClearColor(1, 1, 1, 1);
        glEnable(GL_TEXTURE_2D);
        glLineWidth(4);
    }

    public static boolean update() {
        if (resized) {
            resized = false;
        }
        Display.update();
        if (Display.wasResized()) {
            resized();
            resized = true;
        }
        Display.sync(60);
        return !Display.isCloseRequested();
    }

    /**
     * http://www.java-gaming.org/topics/lwjgl-tutorial-series-a-basic-game/
     * 30674/view.html
     *
     * @author Sri Harsha Chilakapati
     * @param width
     * @param height
     * @param fullscreen
     * @return !resized
     */
    public static boolean setDisplayMode(int width, int height, boolean fullscreen) {
        // return if requested DisplayMode is already set
        if ((Display.getDisplayMode().getWidth() == width) && (Display.getDisplayMode().getHeight() == height) && (Display.isFullscreen() == fullscreen)) {
            return true;
        }
        try {
            // The target DisplayMode
            DisplayMode targetDisplayMode = null;
            if (fullscreen) {
                // Gather all the DisplayModes available at fullscreen
                DisplayMode[] modes = Display.getAvailableDisplayModes();
                int freq = 0;
                // Iterate through all of them
                for (DisplayMode current : modes) {
                    // Make sure that the width and height matches
                    if ((current.getWidth() == width) && (current.getHeight() == height)) {
                        // Select the one with greater frequency
                        if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
                            // Select the one with greater bits per pixel
                            if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
                                targetDisplayMode = current;
                                freq = targetDisplayMode.getFrequency();
                            }
                        }
                        // if we've found a match for bpp and frequency against the
                        // original display mode then it's probably best to go for this one
                        // since it's most likely compatible with the monitor
                        if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) && (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
                            targetDisplayMode = current;
                            break;
                        }
                    }
                }
            } else {
                // No need to query for windowed mode
                targetDisplayMode = new DisplayMode(width, height);
            }
            if (targetDisplayMode == null) {
                return false;
            }
            // Set the DisplayMode we've found
            Display.setDisplayMode(targetDisplayMode);
            Display.setFullscreen(fullscreen);
            // Generate a resized event
            resized();
            return true;
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void resized() {
        double scale = getScale();
        double widthX = Elektoh.WINDOW_DEF_X * scale;
        double widthY = Elektoh.WINDOW_DEF_Y * scale;
        double centerX = (Display.getWidth() / 2d) - (widthX / 2d);
        double centerY = (Display.getHeight() / 2d) - (widthY / 2d);
        glViewport((int) round(centerX), (int) round(centerY), (int) round(widthX), (int) round(widthY));
    }

    public static Coord calibratePointer(Coord pointer) {
        double scale = getScale();
        double widthX = Elektoh.WINDOW_DEF_X * scale;
        double widthY = Elektoh.WINDOW_DEF_Y * scale;
        double centerX = (Display.getWidth() / 2d) - (widthX / 2d);
        double centerY = (Display.getHeight() / 2d) - (widthY / 2d);
        scale = pow(scale, -1);
        int newX = (int) round((pointer.x * scale) - centerX);
        int newY = (int) round((pointer.y * scale) - centerY);
        return new Coord(newX, newY);
    }

    public static double getScale() {
        double widthX = Elektoh.WINDOW_DEF_X;
        double widthY = Elektoh.WINDOW_DEF_Y;
        if (Display.getWidth() >= (Display.getHeight() * (widthX / widthY))) {
            return Display.getWidth() / widthX;
        } else {
            return Display.getHeight() / widthY;
        }
    }

    public static void renderLoadScreen() {
        // TODO
    }

    public static void destroy() {
        Display.destroy();
    }
}
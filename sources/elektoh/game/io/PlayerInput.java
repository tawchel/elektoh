package elektoh.game.io;

import elektoh.game.entities.Speed;
import elektoh.game.utils.Direction;

// TODO Come up with a better name
public class PlayerInput {
    public static boolean active = false;
    public static Speed speed = Speed.WALK;
    public static boolean moving;
    public static Direction facing = Direction.SOUTH;
    public static int runCounter = 0;

    public static void update() {
        if ((runCounter > 130) && (speed == Speed.WALK)) {
            speed = Speed.RUN;
        }
        if (moving && (speed == Speed.WALK)) {
            runCounter++;
        } else if (!moving || (PlayerInput.speed != Speed.RUN)) {
            resetRunCounter();
        }
    }

    public static void resetRunCounter() {
        runCounter = 0;
    }
}
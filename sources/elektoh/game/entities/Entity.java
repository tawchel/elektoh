package elektoh.game.entities;

import static java.lang.Math.*;
import static org.lwjgl.opengl.GL11.*;

import java.util.List;

import elektoh.game.Elektoh;
import elektoh.game.map.World;
import elektoh.game.render.RenderEngine;
import elektoh.game.render.RenderHelper;
import elektoh.game.resource.sprite.Sprite;
import elektoh.game.resource.sprite.SpriteDatabase;
import elektoh.game.tile.Tile;
import elektoh.game.utils.Color;
import elektoh.game.utils.DataObject;
import elektoh.game.utils.Direction;
import elektoh.game.utils.Log;
import elektoh.game.utils.MathUtils;
import elektoh.game.utils.geometry.Coord;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;
import elektoh.game.utils.geometry.Vector;

public abstract class Entity {
    public String entityType;
    public Point position = Point.ORIGIN;
    public boolean noClip = false;
    public World world;
    private Polygon base;
    public double sizeZ = 0;
    public Point prev_position;
    public boolean movable = false;
    public boolean solid = false;

    public Entity(World world) {
        this.world = world;
    }

    public void create(Point position) {
        if (!EntityRegistry.contains(this.getClass())) {
            Log.warning("COULD NOT CREATE " + this.getClass() + " BECAUSE IT HAS NOT BEEN REGISTERED");
            return;
        }
        this.position = position;
        entityType = EntityRegistry.getEntityName(this.getClass());
        world.putEntityInChunk(this);
    }

    public void update() {
        prev_position = position;
    }

    public void moveEntityRelative(Point relPos) {
        if (movable) {
            Point newPosition = position.translate(relPos);
            if (noClip || canMoveOnto(newPosition)) {
                position = newPosition;
            }
        }
    }

    public boolean canMoveOnto(Point pos) {
        boolean yes = true;
        List<Entity> entities = world.getEntities(pos, 1);
        for (Entity entity : entities) {
            if (getBase().translate(pos).translate(getBase().getCenter()).collides(entity.getRelativeBase())) {
                if (solid && entity.solid) {
                    yes = false;
                    doCollide(entity);
                    entity.doCollide(this);
                }
            }
        }
        return yes;
    }

    public void doCollide(Entity entity) {}

    protected boolean walkableTile(Point point) {
        Tile tile = world.getTileUnderPoint(point);
        if (tile == null) {
            return false;
        }
        return tile.walkable;
    }

    public boolean hasPositionNotChanged() {
        return position.equals(prev_position);
    }

    protected void moveDirection(Direction direction, double speed) {
        moveEntityRelative(getFacingRelativePosition(direction, speed));
        if (hasPositionNotChanged() && !direction.primary) {
            switch (direction) {
                case NORTHEAST:
                    moveEntityRelative(getFacingRelativePosition(Direction.NORTH, speed));
                    if (hasPositionNotChanged()) {
                        moveEntityRelative(getFacingRelativePosition(Direction.EAST, speed));
                    }
                    return;
                case SOUTHEAST:
                    moveEntityRelative(getFacingRelativePosition(Direction.SOUTH, speed));
                    if (hasPositionNotChanged()) {
                        moveEntityRelative(getFacingRelativePosition(Direction.EAST, speed));
                    }
                    return;
                case SOUTHWEST:
                    moveEntityRelative(getFacingRelativePosition(Direction.SOUTH, speed));
                    if (hasPositionNotChanged()) {
                        moveEntityRelative(getFacingRelativePosition(Direction.WEST, speed));
                    }
                    return;
                case NORTHWEST:
                    moveEntityRelative(getFacingRelativePosition(Direction.NORTH, speed));
                    if (hasPositionNotChanged()) {
                        moveEntityRelative(getFacingRelativePosition(Direction.WEST, speed));
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private Point getFacingRelativePosition(Direction direction, double speed) {
        Point relPos = Point.ORIGIN;
        switch (direction) {
            case NORTHEAST:
                relPos = relPos.translate(getFacingRelativePosition(Direction.NORTH, speed));
                relPos = relPos.translate(getFacingRelativePosition(Direction.EAST, speed));
                relPos = relPos.scale(0.6d);
                return relPos;
            case SOUTHEAST:
                relPos = relPos.translate(getFacingRelativePosition(Direction.SOUTH, speed));
                relPos = relPos.translate(getFacingRelativePosition(Direction.EAST, speed));
                relPos = relPos.scale(0.9d);
                return relPos;
            case SOUTHWEST:
                relPos = relPos.translate(getFacingRelativePosition(Direction.SOUTH, speed));
                relPos = relPos.translate(getFacingRelativePosition(Direction.WEST, speed));
                relPos = relPos.scale(0.6d);
                return relPos;
            case NORTHWEST:
                relPos = relPos.translate(getFacingRelativePosition(Direction.NORTH, speed));
                relPos = relPos.translate(getFacingRelativePosition(Direction.WEST, speed));
                relPos = relPos.scale(0.9d);
                return relPos;
            default:
                break;
        }
        Vector vec = Vector.createPolar(speed, -direction.angle);
        return new Point(MathUtils.roundTo(5, vec.deltaX()), MathUtils.roundTo(5, vec.deltaY()));
    }

    public Point getPosition() {
        return position;
    }

    public Coord getCoord() {
        return new Coord((int) round(position.x), (int) round(position.y));
    }

    public double getHeight() {
        return sizeZ;
    }

    public void setBase(Polygon base) {
        this.base = base;
    }

    public Polygon getBase() {
        return base;
    }

    public void render(RenderEngine engine) {
        if (Elektoh.DEV_ART) {
            glPushMatrix();
            Polygon transformedBase = getBase().rotate(toRadians(45)).scale(RenderEngine.XY_TILE_WIDTH_HALF, RenderEngine.XY_TILE_HEIGHT_HALF);
            double zHeight = sizeZ * 24;
            glDisable(GL_TEXTURE_2D);
            glPushMatrix();
            double detail = zHeight / 4;
            Color.WHITE.bind();
            for (double d = 0; d < zHeight; d += detail) {
                glTranslated(0, -detail, 0);
                RenderHelper.draw(transformedBase);
            }
            glPopMatrix();
            Color.RED.bind();
            RenderHelper.draw(transformedBase);
            glEnable(GL_TEXTURE_2D);
            glPopMatrix();
        }
    }

    public void save(DataObject data) {
        data.add("entityType", entityType);
        data.add("position", position);
        data.add("noClip", noClip);
    }

    public void load(DataObject data) {
        entityType = data.getString("entityType");
        position = data.getPoint("position");
        noClip = data.getBoolean("noClip");
    }

    public double getXYLayer() {
        double m = -1;
        double yIntercept = position.y - (m * position.x);
        return yIntercept;
    }

    public Polygon getRelativeBase() {
        return getBase().translate(position).translate(getBase().getCenter());
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [" + toStringExtra() + " position=" + position + " ]";
    }

    protected String toStringExtra() {
        return "";
    }

    public void renderEntitySprite(String string) {
        Sprite sprite = SpriteDatabase.getSprite(string);
        glTranslated(-sprite.getRegion().width / 2, -sprite.getRegion().height / 2, 0);
        glTranslated(0, -((sizeZ * 24) / 2), 0);
        sprite.render();
    }
}
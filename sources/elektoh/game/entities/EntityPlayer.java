package elektoh.game.entities;

import elektoh.game.io.Camara;
import elektoh.game.io.PlayerInput;
import elektoh.game.map.World;
import elektoh.game.render.RenderEngine;
import elektoh.game.utils.DataObject;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;

public class EntityPlayer extends EntityLiving implements Camara {
    public String username;

    public EntityPlayer(World world, String username) {
        super(world);
        setBase(Polygon.getSquare(Point.ORIGIN, 0.4d));
        sizeZ = 0.8d;
        this.username = username;
    }

    @Override
    public void create(Point position) {
        super.create(position);
        world.setCamara(this);
    }

    @Override
    public void update() {
        super.update();
        facing = PlayerInput.facing;
        if (PlayerInput.moving) {
            moveFacing(PlayerInput.speed.speed());
            if (hasPositionNotChanged()) {
                PlayerInput.resetRunCounter();
            }
        }
    }

    @Override
    public void render(RenderEngine engine) {
        super.render(engine);
        if (!PlayerInput.moving) {
            renderEntitySprite("human/idle." + facing.abbrv);
        } else {
            renderEntitySprite("human/" + PlayerInput.speed.name().toLowerCase() + "." + facing.abbrv);
        }
    }

    @Override
    public void save(DataObject data) {
        super.save(data);
        data.add("username", username);
    }

    @Override
    public void load(DataObject data) {
        super.load(data);
        username = data.getString("username");
    }

    @Override
    protected String toStringExtra() {
        return " username=" + username + super.toStringExtra();
    }
}
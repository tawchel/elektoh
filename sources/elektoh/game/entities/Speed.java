package elektoh.game.entities;

public enum Speed {
    SNEAK(0.006d),
    WALK(0.025d),
    RUN(0.05d);
    private final double speed;

    Speed(double speed) {
        this.speed = speed;
    }

    public double speed() {
        return speed;
    }
}
package elektoh.game.entities;

import elektoh.game.map.World;
import elektoh.game.utils.geometry.Point;
import elektoh.game.utils.geometry.Polygon;

public class EntityCactus extends EntityDecor {
    public EntityCactus(World world) {
        super(world, "cactus", world.data.rand.nextInt(3));
        solid = true;
        setBase(Polygon.getSquare(Point.ORIGIN, 0.4d));
        sizeZ = 0.8d;
    }
}
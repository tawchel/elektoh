package elektoh.game.entities;

import elektoh.game.map.World;
import elektoh.game.render.RenderEngine;
import elektoh.game.utils.DataObject;

public abstract class EntityDecor extends Entity {
    public String decor;
    public int type;

    public EntityDecor(World world, String decor, int type) {
        super(world);
        this.decor = decor;
        this.type = type;
    }

    @Override
    public void render(RenderEngine engine) {
        super.render(engine);
        renderEntitySprite("decor/" + decor + "." + type);
    }

    @Override
    public void save(DataObject data) {
        super.save(data);
        data.add("decor", decor);
        data.add("type", type);
    }

    @Override
    public void load(DataObject data) {
        super.load(data);
        decor = data.getString("decor");
        type = data.getInteger("type");
    }

    @Override
    protected String toStringExtra() {
        return super.toStringExtra() + " decor=" + decor + " type=" + type;
    }
}
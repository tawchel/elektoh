package elektoh.game.entities;

import elektoh.game.utils.Function;
import elektoh.game.utils.Log;

public class EntityRegistry {
    private static Function<String, Class<? extends Entity>> entities = new Function<String, Class<? extends Entity>>();

    public static void registerEntities() {
        register("EntityPlayer", EntityPlayer.class);
        register("EntityCactus", EntityCactus.class);
    }

    public static boolean register(String name, Class<? extends Entity> entityType) {
        if (entities.put(name, entityType)) {
            return true;
        }
        Log.warning(entityType + " COULD NOT BE REGISTERED AS \"" + name + "\" because the entity has either already been registered, or there is another entity registered under that name.");
        return false;
    }

    public static Class<? extends Entity> getEntityClass(String name) {
        return entities.getFromA(name);
    }

    public static String getEntityName(Class<? extends Entity> entityType) {
        return entities.getFromB(entityType);
    }

    public static boolean contains(Class<? extends Entity> entityType) {
        return entities.contains(entityType);
    }

    public static Entity getEntity(String entityType) {
        Entity entity = null;
        try {
            entity = getEntityClass(entityType).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
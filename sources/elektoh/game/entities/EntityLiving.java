package elektoh.game.entities;

import elektoh.game.map.World;
import elektoh.game.utils.DataObject;
import elektoh.game.utils.Direction;
import elektoh.game.utils.geometry.Point;

public abstract class EntityLiving extends Entity {
    public Direction facing;

    public EntityLiving(World world) {
        super(world);
        movable = true;
        facing = Direction.SOUTHEAST;
        solid = true;
    }

    @Override
    public boolean canMoveOnto(Point pos) {
        boolean yes = super.canMoveOnto(pos);
        for (Point point : getBase().getPoints()) {
            if (!walkableTile(point.translate(pos))) {
                yes = false;
            }
        }
        return yes;
    }

    public void moveFacing(double speed) {
        moveDirection(facing, speed);
    }

    @Override
    public void save(DataObject data) {
        super.save(data);
        data.add("facing", facing.ordinal());
    }

    @Override
    public void load(DataObject data) {
        super.load(data);
        facing = Direction.values()[data.getInteger("facing")];
    }

    @Override
    protected String toStringExtra() {
        return super.toStringExtra() + " facing=" + facing;
    }
}